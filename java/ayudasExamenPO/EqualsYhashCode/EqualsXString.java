@Override
public boolean equals(Object obj) {
    if (this == obj)
        return true;
    if (obj == null)
        return false;
    if (getClass() != obj.getClass())
        return false;
    Flight other = (Flight) obj;
    if (code == null) {
        if (other.code != null)
            return false;
    } else if (!code.equals(other.code))
        return false;
    return true;
}