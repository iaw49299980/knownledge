/*
 * Player.java        1.0 22/02/2012
 *
 * Models the program.
 *
 * Copyright 2012 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *                Joaquim Laplana Tarragona <jlaplana@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.Period;

public class Player {
  
  /** Nom del jugador */
  private String name;
  /** Data de naixement del jugador */
  private LocalDateTime birthday;
  /** Data de mort del jugador */
  private LocalDateTime deathday;
  /** Targeta de "goodies" del jugador */
  public GoodiesCard goodiesCard;
  /** Temps de vida inicial d'un jugador, en minuts */
  private static final int INITIAL_TIME_OF_LIFE = 5;
  /** Temps de vida pel qual es pot intercanviar un superpoder, en minuts */
  private static final int SUPER_POWER_LIFE_VALUE = 10;
  /** Quilocalories diàries que necessita un jugador */
  private static final double KCAL_PER_DAY = 2200;
  /** Preu d'1 quilocaloria */
  private static final int KCAL_PRICE = 2;
  /** Valor en punts d'alguns "goodies" */
  private static final int EXTRAS_VALUE = 15;
  
  /**
   * Constructor de la classe Player. Quan es crea un jugador es complirà: 
   *   - La data de naixement és l'hora actual. 
   *   - La data de mort és d'aquí a INITIAL_TIME_OF_LIFE minuts. 
   *   - Se li crearà una targeta de "goodies" per defecte.
   * 
   * @param name el nom del jugador
   */
  public Player(String name) {
    // TODO
    this.name = name;
    this.birthday =  LocalDateTime.now();
    this.deathday =  LocalDateTime.of(this.birthday.getYear(), this.birthday.getMonthValue(), this.birthday.getDayOfMonth(), this.birthday.getHour(), this.birthday.getMinute()+INITIAL_TIME_OF_LIFE, this.birthday.getSecond()); 
    this.goodiesCard = new GoodiesCard();
  }
  public LocalDateTime getBirthday() {
    return this.birthday;
  }
  public LocalDateTime getDeathday() {
    return this.deathday;
  }
  /**
   * Calcula el temps de vida del jugador, des de que neix fins que mor.
   * 
   * @return la quantitat de segons de vida del jugador.
   */
  public long timeOfLife() {
    // TODO
    return JodaDT.durationInSeconds(this.birthday, this.deathday);
  }
  /**
   * Determina si el jugador està viu.
   * 
   * @return true si està viu, false altrament
   */
  public boolean isAlive() {
    // TODO
    LocalDateTime now = LocalDateTime.now();
    return JodaDT.durationInSeconds(now, this.deathday) > 0 ? true : false;
  }
  /**
   * Calcula quant temps de vida li queda al jugador. Si el jugador està
   * mort no té temps de vida.
   * 
   * @return la quantitat de segons de vida que li queden al jugador.
   */
  public long restOfLife() {
    // TODO
    LocalDateTime now = LocalDateTime.now();
    return JodaDT.durationInSeconds(now, this.deathday) > 0 ? JodaDT.durationInSeconds(now, this.deathday) : 0;
  }
  /**
   * Intercanvia -n- superpoders per temps de vida. Cada superpoder val
   * SUPER_POWER_LIFE_VALUE minuts de vida.
   * 
   * Per tal de fer l'intercanvi s'ha de comprovar que: 
   *   - el jugador està viu
   *   - el jugador disposa com a mínim de -n- superpoders
   * 
   * @param n nombre de superpoders per intercanviar
   * @return true si s'ha fet l'intercanvi, false altrament
   */
  public boolean superPower2life(int n) {
    // TODO
    if (!isAlive()) {
      return false;
    }
    else if(goodiesCard.getSuperPower() < n) {
      return false;
    }
    int tempsMes = n * SUPER_POWER_LIFE_VALUE;
    int superPodersRestants = goodiesCard.getSuperPower() - n;
    goodiesCard.setSuperPower(superPodersRestants);
    this.deathday = deathday.plusMinutes(tempsMes);
    return true;
  }
  /**
   * Calcula la quantitat de quilocalories que necessita un jugador des d'ara
   * fins el dia de la seva mort. Si el jugador està mort, no necessita
   * quilocalories.
   * 
   * @return el nombre quilocalories
   */
  public double kcalNeeded() {
    // TODO
    if (!isAlive()) {
      return 0;
    }
    double kcalXseg = 0.02546296296296296;
    return restOfLife() * kcalXseg;
  }
  /**
   * Compra -food- quilocalories (amb diners).
   * 
   * Per tal de fer l'intercanvi s'ha de comprovar que: 
   *   - el jugador està viu
   *   - el jugador disposa dels diners necessaris per comprar les quilocalories
   * demanades.
   * 
   * @param food quantitat de quilocalories que vol comprar
   * @return true si s'ha efectuat la compra, false altrament
   */
  public boolean buyFood(long food) {
    // TODO
    if (!isAlive()) {
      return false;
    }
    double preu = food * KCAL_PRICE;
    if (goodiesCard.getMoney() < preu) {
      return false;
    }
    double money = goodiesCard.getMoney() - preu;
    goodiesCard.setMoney(money);
    return true;
  }
  /**
   * Intercanvia menjar per salut. Per aconseguir el 'goodie' de salut, el
   * jugador necessita donar les quilocalories que necessita per viure el que
   * li queda de vida.
   * 
   * Per tal de fer l'intercanvi s'ha de comprovar que: 
   *   - el jugador està viu
   *   - el jugador disposa de les quilocalories necessàries per aconseguir el
   * 'goodie' de la salut.
   * 
   * @return true si s'ha aconseguit el goodie salut, false altrament
   */
  public boolean food2health() {
    if (!isAlive()) {
      return false;
    }
    double kcalneed = kcalNeeded();
    if (goodiesCard.getFood() < kcalneed) {
      return false;
    }
    double food = goodiesCard.getFood() - kcalneed;
    goodiesCard.setFood(food);
    goodiesCard.setHealth(true);
    return true;
    
  }
  /**
   * Perd els següents goodies: 
   *   - salut 
   *   - amor 
   *   - tots els diners 
   *   - tots els superpoders
   */
  public void looseSomeGoodies() {
    // TODO
    goodiesCard.setFood(0);
    goodiesCard.setMoney(0);
    goodiesCard.setSuperPower(0);
    goodiesCard.setHealth(false);
    goodiesCard.setLove(false);
  }
  /**
   * Guanya diners.
   * 
   * @param money nombre d'euros que s'acumulen als que ja té el jugador.
   */
  public void winMoney(double money) {
    // TODO
    double addMoney = goodiesCard.getMoney() + money;
    goodiesCard.setMoney(addMoney);
  }
  /**
   * El jugador mor ara mateix.
   */
  public void gameover() {
    // TODO
    deathday = LocalDateTime.now();
  }
  /**
   * Calcula el nombre de punts del jugador. Els punts es calculen de la
   * següent manera: 
   *   - Cada superpoder val 1 punt 
   *   - L'amor i la salut valen Player.EXTRAS_VALUE punts 
   *   - Si no es té salut es resten Player.EXTRAS_VALUE punts 
   *   - Els diners donen tant punts com euros es tinguin sense decimals 
   *   (truncant a la baixa)
   * 
   * NO es pot retornar un valor negatiu. Si tenim punts negatius, es
   * considera que tenim 0 punts.
   * 
   * @return el nombre de punts.
   */
  public int points() {
    // TODO
    int points = 0;
    int amor =  goodiesCard.getLove() ? 1 : 0;
    int health =  goodiesCard.getHealth() ? 1 : 0;
    points += goodiesCard.getSuperPower();
    points += (amor + health) * EXTRAS_VALUE;
    points -= amor == 0 ?  EXTRAS_VALUE : 0;
    points -= health == 0 ?  EXTRAS_VALUE : 0;
    points += Math.floor(goodiesCard.getMoney());
    return points > 0 ? points : 0; 
  }
  /**
   * Genera un informe del jugador amb el següent format:
   * 
   * JUGADOR: Laura 
   * ================= 
   * Data de naixement: 23/02/2012-21:12 
   * Data de mort: 23/02/2012-21:17 
   * Temps de vida restant (mm:ss): 4:27 
   * Salut: no
   * Amor: sí 
   * Diners: 100.0€ 
   * Menjar: 1000.0kcal 
   * Superpoders: 3 
   * Punts: 118
   * 
   */
  public void reportStatusPlayer() {
    // TODO
    // Get variables
    String birthdayFormat = JodaDT.formatDDMMYYYYhhmm(birthday);
    String deathdayFormat = JodaDT.formatDDMMYYYYhhmm(deathday);
    long tempsVidaSeg = restOfLife();
    long minVida = tempsVidaSeg / 60;
    long segVida = tempsVidaSeg - (minVida * 60);
    String vida = minVida + ":" + segVida;
    String salut = goodiesCard.getHealth() ? "Si" : "No";
    String amor = goodiesCard.getLove() ? "Si" : "No";
    double money = goodiesCard.getMoney();
    double food = goodiesCard.getFood();
    int poders = goodiesCard.getSuperPower();
    int points = points();
    System.out.printf("JUGADOR: %s\n", name);
    System.out.printf("====================\n");
    System.out.printf("Data de naixement: %s\n", birthdayFormat);
    System.out.printf("Data de mort: %s\n", deathdayFormat);
    System.out.printf("Temps de vida restant (mm:ss): %s\n", vida);
    System.out.printf("Salut: %s\n", salut);
    System.out.printf("Amor: %s\n", amor);
    System.out.printf("Money: %.1f �\n", money);
    System.out.printf("Menjar: %.1f kcal\n", food);
    System.out.printf("Poders: %d \n", poders);
    System.out.printf("Punts: %d \n", points);
  }
}
