/*
 * Route.java        1.0 22/03/2012
 *
 * Modela una ruta entre dues estacions d'una línia de metro.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.time.LocalDateTime;

public class Route {

    /** La data i hora de sortida en format DD/MM/YYYY-hh:mm */
    private String departureTime;
    /** El nom de l'estació de sortida */
    private String departureStationName;
    /** El nom de l'estació d'arribada */
    private String arrivalStationName;
    /** La línia per on passa la ruta */
    private Line line;

    // Constructor
    public Route(String departureTime, String departureStationName, String arrivalStationName) {
        this.departureTime = departureTime;
        this.departureStationName = departureStationName;
        this.arrivalStationName = arrivalStationName;
    }

    /**
     * Calcula el nombre d'estacions per les quals passa la ruta, incloses les estacions inicial i final.
     * 
     * @return el nombre d'estacions.
     */
    public int numberOfStations() {
        // TODO
        int start = line.getPositionStation(departureStationName);
        int finish = line.getPositionStation(arrivalStationName);
        if (start > finish) {
            return start - finish;
        } else {
            return finish - start;
        }
    }

    /**
     * Calcula quant de temps (màxim) es triga en recórrer la ruta,
     * tenint en compte el temps d'espera del metro.
     * 
     * @return el temps, en minuts.
     */
    public double time() {
        // TODO
        int numberOfStations = numberOfStations();
        double sumatori = 0;
        int tempsEsperaMetro = 0;
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime horaPuntaInici = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 8, 0);
        LocalDateTime horaPuntaFi = LocalDateTime.of(now.getYear(),now.getMonth(), now.getDayOfMonth(), 9, 59);
        System.out.println(horaPuntaInici);
        if (JodaDT.isInInterval(JodaDT.parseDDMMYYYYhhmm(departureTime), horaPuntaInici, horaPuntaFi)) {
            sumatori += line.FREQUENCY_RUSH_HOUR;
        }
        for(int i = 0; i < numberOfStations; i++) {
            sumatori += line.TIME_BETWEEN_STATIONS;
        }
        return sumatori;
    }

    /**
     * Calcula la data i hora estimada d'arribada de la ruta.
     * 
     * @return la dta i hora en format DD/MM/YYYY-hh:mm
     */
    public String arrivalTime() {
        // TODO
        LocalDateTime arrivalTime = JodaDT.parseDDMMYYYYhhmm(departureTime).plusMinutes((long) time());
        return JodaDT.formatDDMMYYYYhhmm(arrivalTime);
        
    }

    // Setter
    public void setLine(Line line) {
        this.line = line;
    }

}
