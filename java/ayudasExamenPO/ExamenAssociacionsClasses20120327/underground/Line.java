/*
 * Line.java        1.0 22/03/2012
 *
 * Modela una línia de metro.
 *
 * Copyright 2012  NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.ArrayList;
import java.util.HashSet;

public class Line {
    
    /** Número de l'estació */
    private int number;
    /** Estacions de la línia */
    private ArrayList<Station> stations;
    /** Cada quants minuts passa un metro per la línia en hora punta, 08:00-09:59, 17:00-18:59 */
    public static final double FREQUENCY_RUSH_HOUR = 1;
    /** Cada quants minuts passa un metro per la línia en hores normals */
    public static final double FREQUENCY_NORMAL_HOUR = 3;
    /** Temps que tiga un metro en anar des d'una estació a l'estació següent */
    public static final double TIME_BETWEEN_STATIONS = 1.5;
    
    // Constructor
    public Line(int number) {
        this.number = number;
        this.stations = new ArrayList<Station>();
    }
    
    /**
     * Afegeix una estació a la línia.
     * 
     * @param name el nom de l'estació a afegir.
     * @param entranceNames els noms de les entrades de l'estació a afegir.
     * @return true si s'ha pogut afegir, false altrament.
     */
    public boolean addStation(String name, String[] entranceNames) {
        // TODO
        Station s = new Station(name);
        s.addEntrances(entranceNames);
        return stations.add(s);
    }
    
    /**
     * Recupera la posició d'una estació de la línia. La primera estació està a la posició 1, 
     * la segona a la posició 2 i així consecutivament.
     * 
     * @param stationName el nom de l'estació
     * @return la posició en la línia de l'estació de nom 'stationName' o -1 si l'estació no existeix 
     */
    public int getPositionStation(String stationName) {
        // TODO
        Station s = new Station(stationName);
        return stations.indexOf(s);
    }
    
    /**
     * Construeix un HashSet d'estacions amb les estacions de la línia que tenen entrades de nom 
     * 'entranceName'
     * 
     * @param entranceName un nom d'entrada
     * @return el conjunt d'estacions
     */
    public HashSet<Station> searchStationsWithEntrance(String entranceName) {
        // TODO
        HashSet<Station> sts = new HashSet<Station>();
        for(int i = 0; i < stations.size(); i++) {
            for (Entrance e : stations.get(i).getEntrances()) {
                if(e.getName().equals(entranceName)) {
                    sts.add(stations.get(i));
                }
            }
        }
        return sts;
    }
    
    /**
     * Imprimeix per pantalla totes les estacions (amb les seves entrades) de la línia. 
     * El format és el següent:
     * 
     * Línia 3
     * -------
     * 1 - Zona Universitària - Accés: Avinguda Diagonal, Accés: Facultat d'Empresarials
     * 2 - Palau Reial - Accés: Av. Diagonal / Martí i Franquès, Accés: Av. Diagonal / Museus, Accés: Av. Diagonal / Palau de Pedralbes, Accés: Av. Diagonal / Facultat de Ciències
     * 3 - Maria Cristina - Accés: Avinguda Diagonal / Doctor Ferran, Accés: Gran Via Carles III, Accés: Avinguda Diagonal
     * 4 - Les Corts - Accés: Travessera de les Corts, Accés: Joan Güell
     * ...
     */
    public void reportStationsEntrances() {
        // TODO
        System.out.printf("Línea %d\n", number);
        for(int i = 0; i < stations.size(); i++) {
            System.out.printf("%d - %s\n", i, stations.get(i));
        }
    }
    
    /**
     * Imprimeix per pantalla totes les estacions (
     * amb les seves entrades) tals que tinguin una entrada de nom 'entranceName'. 
     * El format és el següent (suposant que entranceName és 'Avinguda Diagonal'):
     * 
     * Zona Universitària - Accés: Avinguda Diagonal, Accés: Facultat d'Empresarials
     * Maria Cristina - Accés: Avinguda Diagonal / Doctor Ferran, Accés: Gran Via Carles III, Accés: Avinguda Diagonal
     * 
     * @param entranceName
     */
    public void reportStationsWithEntrance(String entranceName) {
        // TODO
        HashSet<Station> sts = searchStationsWithEntrance(entranceName);
        for (Station s : sts) {
            System.out.println(s);
        }
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + number;
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Line other = (Line) obj;
        if (number != other.number)
            return false;
        return true;
    }   
    
    public ArrayList<Station> getStations() {
        return stations;
    }
}
