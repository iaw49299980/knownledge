/*
 * Entrance.java        1.0 22/03/2012
 *
 * Modela una entrada a una estació de metro.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Entrance {

    /** Nom de l'accés o entrada */
    private String name;

    // Constructor
    public Entrance(String name) {
        this.name = name;
    }

    /**
     * Construeix una cadena amb el següent format:
     * 
     * Accés: nomEntrada
     * 
     * Per exemple:
     * 
     * Accés: Avinguda Diagonal
     * 
     * @return la cadena
     */
    @Override
    public String toString() {
        return "Accés: " + name;
    }

    // Getter
    public String getName() {
        return name;
    }
}
