/*
 * Station.java        1.0 22/03/2012
 *
 * Modela una estació de metro.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.HashSet;

public class Station {

    /** El nom de l'estació */
    private String name;
    /** Les entrades del carrer per tal d'accedir a l'estació */
    private HashSet<Entrance> entrances;

    // Constructor
    public Station(String name) {
        this.name = name;
        this.entrances = new HashSet<Entrance>();
    }

    /**
     * Per a cada nom d'entrada, es crea l'entrada corresponent i s'afegeix a les entrades de l'estació.
     * 
     * @param entranceNames un array amb noms d'entrada.
     */
    public boolean addEntrances(String[] entranceNames) {        
        // TODO
        Entrance newEntrance;
        for (int i = 0; i < entranceNames.length; i++) {
           newEntrance = new Entrance(entranceNames[i]);
           this.entrances.add(newEntrance);
        }
        return true;
    }

    /**
     * Construeix una cadena amb el següent format:
     * 
     * nomEstacio - Accés: nomEntrada1, Accés: nomEntrada2, Accés: nomEntrada3...
     * 
     * Per exemple:
     * 
     * Maria Cristina - Accés: Avinguda Diagonal / Doctor Ferran, Accés: Gran Via Carles III, Accés: Avinguda Diagonal
     * 
     * @return la cadena
     */
    @Override
    public String toString() {
        // TODO
        String estacio = this.name + " - ";
        for (Entrance e : this.entrances) {
            estacio += e.toString();
            estacio += ", ";
        }
        estacio = estacio.substring(0, estacio.length()-2);
        return estacio;
    }

    // equals i hashCode

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Station other = (Station) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    // Getters

    public String getName() {
        return name;
    }
    public HashSet<Entrance> getEntrances() {
        return entrances;
    }

}
