/*
 * Main.java        1.0 23/03/2012
 *
 * Models the program.
 *
 * Copyright 2012 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *                Joaquim Laplana Tarragona <jlaplana@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Main {

    /** Línia de metro */
    private Line line;

    // Constructor
    private Main() {
        this.generateLine();
    }

    /**
     * Generació de dades d'exemple
     */
    private void generateLine() {
        // Càrrega de dades
        this.line = new Line(3);
        String[] entranceNames1 = { "Avinguda Diagonal", "Facultat d'Empresarials" };
        this.line.addStation("Zona Universitària", entranceNames1);
        String[] entranceNames2 = { "Av. Diagonal / Facultat de Ciències", "Av. Diagonal / Museus",
                "Av. Diagonal / Martí i Franquès", "Av. Diagonal / Palau de Pedralbes" };
        this.line.addStation("Palau Reial", entranceNames2);
        String[] entranceNames3 = { "Gran Via Carles III", "Avinguda Diagonal", "Avinguda Diagonal / Doctor Ferran" };
        this.line.addStation("Maria Cristina", entranceNames3);
        String[] entranceNames4 = { "Joan Güell", "Travessera de les Corts" };
        this.line.addStation("Les Corts", entranceNames4);
        String[] entranceNames5 = { "Plaça del Centre", "Plaça del Centre / Saleta" };
        this.line.addStation("Plaça del Centre", entranceNames5);
        String[] entranceNames6 = { "Pl. Països Catalans / Numància / Ambulatori", "Enric Bargés",
                "Av. Josep Tarradellas", "Numància" };
        this.line.addStation("Sants Estació", entranceNames6);
        String[] entranceNames7 = { "València", "Sant Nicolau", "Tarragona / Elisi", "Aragó" };
        this.line.addStation("Tarragona", entranceNames7);
        String[] entranceNames8 = { "Exposició", "Avinguda Paral·lel" };
        this.line.addStation("Espanya", entranceNames8);
        String[] entranceNames9 = { "Manso", "Parlament", "Teodor Bonaplata" };
        this.line.addStation("Poble Sec", entranceNames9);
        String[] entranceNames10 = { "Fontrodona", "Fontrodona", "Avinguda Paral·lel", "Avinguda Paral·lel" };
        this.line.addStation("Paral·lel", entranceNames10);
        String[] entranceNames11 = { "Drassanes" };
        this.line.addStation("Drassanes", entranceNames11);
        String[] entranceNames12 = { "Teatre Liceu / via 2", "Mercat de la Boqueria / via 2", "Teatre Liceu / via 1",
                "Mercat de la Boqueria / via 1" };
        this.line.addStation("Liceu", entranceNames12);
        String[] entranceNames13 = { "La Rambla / Rotonda", "Plaça Catalunya / Rivadeneyra", "Rambla Catalunya / Pelai" };
        this.line.addStation("Catalunya", entranceNames13);
        String[] entranceNames14 = { "Aragó / Pau Claris", "Aragó / Rambla", "Aragó / Renfe", "Consell Cent",
                "Aragó / Rodalies" };
        this.line.addStation("Passeig de Gràcia", entranceNames14);
        String[] entranceNames15 = { "Pau Claris / Rosselló", "Diagonal / Rbla. Catalunya", "Diagonal / Pau Claris",
                "Rosselló / Rambla Catalunya" };
        this.line.addStation("Diagonal", entranceNames15);
        String[] entranceNames16 = { "Gran de Gràcia" };
        this.line.addStation("Fontana", entranceNames16);
        String[] entranceNames17 = { "Plaça Lesseps" };
        this.line.addStation("Lesseps", entranceNames17);
        String[] entranceNames18 = { "Avinguda República Argentina", "Avinguda Vallcarca / Gomis" };
        this.line.addStation("Vallcarca", entranceNames18);
        String[] entranceNames19 = { "Avinguda Vallcarca", "Anna Piferrer" };
        this.line.addStation("Penitents", entranceNames19);
        String[] entranceNames20 = { "Les Basses d'Horta", "Avinguda Jordà" };
        this.line.addStation("Vall d'Hebron", entranceNames20);
        String[] entranceNames21 = { "Ronda de Dalt / Pare Mariana", "Jardins Pedro Muñoz Seca" };
        this.line.addStation("Montbau", entranceNames21);
        String[] entranceNames22 = { "Passeig Vall d'Hebron", "Can Marcet", "Salesians" };
        this.line.addStation("Mundet", entranceNames22);
        String[] entranceNames23 = { "Passeig Valldaura" };
        this.line.addStation("Valldaura", entranceNames23);
        String[] entranceNames24 = { "García Lorca", "Via Favència" };
        this.line.addStation("Canyelles", entranceNames24);
        String[] entranceNames25 = { "Vidal i Guasch", "Les Torres", "Jaume Pinent" };
        this.line.addStation("Roquetes", entranceNames25);
        String[] entranceNames26 = { "Palamós" };
        this.line.addStation("Trinitat Nova", entranceNames26);
    }

    /**
     * Execució de l'exemple.
     */
    private void execute() {
        // llistat de totes les estacions de la línia
        this.line.reportStationsEntrances();

        // Estacions que tenen una entrada concreta
        System.out.println("\nEstacions amb entrada 'Avinguda Diagonal'");
        this.line.reportStationsWithEntrance("Avinguda Diagonal");

        System.out.println("\nEstacions amb entrada 'Avinguda Paral·lel'");
        this.line.reportStationsWithEntrance("Avinguda Paral·lel");

        // Exemples de rutes
        Route r = new Route("23/02/2012-08:52", "Maria Cristina", "Poble Sec");
        r.setLine(this.line);
        int stops = r.numberOfStations();
        double minutes = r.time();
        String at = r.arrivalTime();
        System.out.println("\nRuta des de Maria Cristina a Poble Sec a les 23/2/2012-08:52: " + stops + " parades, "
                + minutes + " minuts, arribada: " + at + ".");

        r = new Route("23/02/2012-08:52", "Poble Sec", "Maria Cristina");
        r.setLine(this.line);
        stops = r.numberOfStations();
        minutes = r.time();
        at = r.arrivalTime();
        System.out.println("Ruta des de Poble Sec a Maria Cristina a les 23/2/2012-08:52: " + stops + " parades, "
                + minutes + " minuts, arribada: " + at + ".");

        r = new Route("23/02/2012-10:52", "Poble Sec", "Maria Cristina");
        r.setLine(this.line);
        stops = r.numberOfStations();
        minutes = r.time();
        at = r.arrivalTime();
        System.out.println("Ruta des de Poble Sec a Maria Cristina a les 23/2/2012-10:52: " + stops + " parades, "
                + minutes + " minuts, arribada: " + at + ".");
    }

    public static void main(String[] args) {
        Main m = new Main();
        m.execute();
    }

}
