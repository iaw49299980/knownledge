/*
 * Airport.java
 * 
 * Copyright 2010 Pablo Hernando <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


import java.util.HashSet;

/**
 * Modelizes a Airport.
 */
public class Airport {
    
    private String code;
    private String name;
    private String lat;
    private String lon;
    private double gmt;
    
    
    /**
     * Constructor.
     */
    public Airport() {
        this.code = "123";
    }
    public Airport(String code) {
        this.code = code;
    }
    public Airport(String code, String name, String lat, String lon, String gmt) {
        this.code = code;
        this.name = name;
        this.lat = lat;
        this.lon = lon;
        this.gmt = gmt;
    }
    public double distance(Airport a2) {
        
        
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + code.hashCode();
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Airport other = (Airport) obj;
        if (code == null) {
            if (other.code != null)
                return false;
        } else if (!code.equals(other.code))
            return false;
        return true;
    }
    public String getCode() {
        return this.code;
    }
    public String getName() {
        return this.name;
    }
    public String getLat() {
        return this.lat;
    }
    public String getLon() {
        return this.lon;
    }
    public String getGmt() {
        return this.gmt;
    }
}