/*
 * Plane.java
 * 
 * Copyright 2010 Pablo Hernando <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


import java.util.HashSet;

/**
 * Modelizes a Plane.
 */
public class Plane {
    
    private String code;
    private String model;
    private double v;
    
    
    /**
     * Constructor.
     */
    public Plane() {
        this.code = "123";
    }
    public Plane(String code) {
        this.code = code;
    }
    public Plane(String code, String model, double v) {
        this.code = code;
        this.model = model;
        this.v = v;
    }
    
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + code.hashCode();
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Plane other = (Plane) obj;
        if (code == null) {
            if (other.code != null)
                return false;
        } else if (!code.equals(other.code))
            return false;
        return true;
    }
    public String getCode() {
        return this.code;
    }
    public String getModel() {
        return this.model;
    }
    public String getV() {
        return this.v;
    }
}