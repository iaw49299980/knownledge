/*
 * Surgery.java
 * 
 * Copyright 2010 Pablo Hernando <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


import java.time.LocalDateTime;
import java.util.HashSet;

/**
 * Modelizes a Surgery.
 */
public class Surgery {
    
    private String doctorName;
    private String speciallity;
    // Almatzenem totes les visites
    private HashSet<Visit> visits;
    
    /**
     * Constructor.
     */
    public Surgery() {
        this.visits = new HashSet<Visit>();
    }
    public Surgery(String doctorName, String speciallity) {
        this.doctorName= doctorName;
        this.speciallity = speciallity;
        this.visits = new HashSet<Visit>();
    }
    /**
     * Comprova que no hi hagui una visita a la mateixa hora.
     * 
     * @return true || false
     */
    public boolean isProgrammable (Visit visit) {
        boolean is = this.visits.contains(visit);
        return is;
    }
    /**
     * Programa una visita.
     * 
     * 
     * @return true || false
     */
    public boolean program (Visit visit) {
        if (!isProgrammable(visit)) {
            return this.visits.add(visit);
        }
        return false;
    }
    /**
     * Compte les visites que té.
     * 
     * @return visites
     */
    public int countVisits() {
        return this.visits.size();
    }
    /**
     * Compte les visites que té en un període concret.
     * 
     * @return visites
     */
    public int countVisits(String date1, String date2) {
        LocalDateTime interval1 = JodaDT.parseDDMMYYYYhhmm(date1);
        LocalDateTime interval2 = JodaDT.parseDDMMYYYYhhmm(date2);
        int count = 0;
        for (Visit v : this.visits) {
            if(JodaDT.isInInterval(JodaDT.parseDDMMYYYYhhmm(v.getVisitDateTime()), interval1, interval2)) {
                count++;
            }
        }
        return count;
    }
    
    /**
     * Cancela una visita
     * 
     * @return true || false
     */
    
    public boolean cancel(Visit visit) {
        return this.visits.remove(visit);
    }
    
    /**
     * Cancela totes les visites
     * 
     */
    
    public void cancelAll() {
        this.visits.clear();
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((doctorName == null) ? 0 : doctorName.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Surgery other = (Surgery) obj;
        if (doctorName == null) {
            if (other.doctorName != null)
                return false;
        } else if (!doctorName.equals(other.doctorName))
            return false;
        return true;
    }
    /**
     * getters y setters
     * 
     */
    public String getDoctorName() {
        return this.doctorName;
    }
}