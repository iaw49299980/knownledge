/*
 * Visit.java
 * 
 * Copyright 2010 Pablo Hernando <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.time.LocalDateTime;
import java.util.HashSet;

/**
 * Modelizes a Surgery.
 */
public class Visit {
    
    private String visitDateTime;
    private String patientName;
    
    /**
     * Constructor.
     */
    public Visit() {
        this.visitDateTime = JodaDT.formatDDMMYYYYhh(LocalDateTime.now());
    }
    public Visit(String visitDateTime) {
        this.visitDateTime = visitDateTime;
    }
    public Visit(String visitDateTime, String patientName) {
        this.visitDateTime = visitDateTime;
        this.patientName = patientName;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((visitDateTime == null) ? 0 : visitDateTime.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Visit other = (Visit) obj;
        if (visitDateTime == null) {
            if (other.visitDateTime != null)
                return false;
        } else if (!visitDateTime.equals(other.visitDateTime))
            return false;
        return true;
    }
    /**
     * Getters y setters
     */
    public String getVisitDateTime() {
        return this.visitDateTime;
    }
    
    public void setVisitDateTime(String visitDateTime) {
        this.visitDateTime = visitDateTime;
    }

    public String getPatientName() {
        return this.patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }
}