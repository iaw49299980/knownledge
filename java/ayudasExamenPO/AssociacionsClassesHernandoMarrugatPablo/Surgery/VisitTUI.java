/*
 * VisitTUI.java
 * 
 *
 */


import java.util.HashSet;

public class VisitTUI {

    public static void main(String[] args) {
        
        Visit v1 = new Visit("19/03/2019-20:19");
        Visit v2 = new Visit("20/03/2019-20:19");
        Visit v3 = new Visit("19/03/2019-20:19");
        Visit v4 = new Visit("18/03/2019-20:19");
        Surgery s1 = new Surgery();
        s1.program(v1);
        s1.program(v2);
        s1.program(v3);
        s1.program(v4);
        
        System.out.println(s1.countVisits("19/03/2019-21:19", "21/03/2019-20:19"));
        System.out.println(s1.countVisits());
        System.out.println(s1.cancel(v1));
        System.out.println(s1.countVisits());
        s1.cancelAll();
        System.out.println(s1.countVisits());
    } 
   
}