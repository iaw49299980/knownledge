/*
 * Contract.java
 * 
 * Copyright 2010 Pablo Hernando <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.time.LocalDateTime;
import java.util.HashSet;

/**
 * Modelizes a Contract.
 */
public class Contract {
    
    private int code;
    private String initialDate;
    private String endDate;
    Company company;
    
    /**
     * Constructor.
     */
    public Contract() {
        //this.visitDateTime = JodaDT.formatDDMMYYYYhh(LocalDateTime.now());
    }
    public Contract(int code) {
        this.code = code;
    }
    public Contract(int code, String initialDate, String endDate) {
        this.code = code;
        this.initialDate = initialDate;
        this.endDate = endDate;
    }
    public int duration() {
        int dias = (int) (JodaDT.durationInSeconds(JodaDT.parseDDMMYYYYhh(initialDate), JodaDT.parseDDMMYYYYhh(endDate)) / 86400);
        return dias;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + code;
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Contract other = (Contract) obj;
         if (this.code != other.code)
            return false;
        return true;
    }
    /**
     * Getters y setters
     */
    public Company getCompany() {
        return this.company;
    }
    public String getInitialDate() {
        return this.initialDate;
    }
    public String getEndDate() {
        return this.endDate;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
}