/*
 * Person.java
 * 
 * Copyright 2010 Pablo Hernando <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


import java.time.LocalDateTime;
import java.util.HashSet;

/**
 * Modelizes a Person.
 */
public class Person {
    
    private int dni;
    private String name;
    
    // Almatzenem tots els contractes
    private HashSet<Contract> contracts;
    
    /**
     * Constructor.
     */
    public Person() {
        this.contracts = new HashSet<Contract>();
    }
    public Person(int dni) {
        this.dni= dni;
        this.contracts = new HashSet<Contract>();
    }
    
    public int workedTime() {
        int sumatoriDies = 0;
        for (Contract c : this.contracts) {
            sumatoriDies += c.duration();
        }
        return sumatoriDies;
    } 
    
    /**
     * Compte les empreses en les que ha treballat.
     * 
     * @return visites
     */
    public int nCompanies() {
        int [] empresasContabilitzada = new int[this.contracts.size()];
        int empresaMida = this.contracts.size();
        int actualEmpresa = -1;
        int empresas = 0;
        int empresasDiferentes = 0;
        boolean trobada = false;
        for (Contract c : this.contracts) {
            actualEmpresa = c.getCompany().getCif();
            for(int i = 0; i < empresaMida && !trobada; i++) {
                if(actualEmpresa == empresasContabilitzada[i]) {
                    trobada = true;
                }
            }
            if (!trobada) {
                empresasContabilitzada[empresasDiferentes] = actualEmpresa;
                empresasDiferentes++;
                trobada = false;
            }
        }
        return empresasDiferentes;
    }
    /**
     * Compte en quantes empreses diferents ha treballat
     * 
     */
    public boolean signContract (Contract contract) {
        return this.contracts.add(contract);
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + dni;
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Person other = (Person) obj;
        if (dni != other.dni)
            return false;
        return true;
    }
}