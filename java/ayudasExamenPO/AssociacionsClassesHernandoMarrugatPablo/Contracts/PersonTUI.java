/*
 * PersonTUI.java
 * 
 *
 */


import java.util.HashSet;

public class PersonTUI {

    public static void main(String[] args) {
        
        Company c1 = new Company(1);
        Company c2 = new Company(2);
        Company c3 = new Company(3);
        Person p1 = new Person(49299980);
        Contract t1 = new Contract(1, "19/03/2019-20", "21/03/2019-20");
        Contract t2 = new Contract(2, "19/03/2019-20", "21/03/2019-20");
        Contract t3 = new Contract(3, "19/03/2019-20", "21/03/2019-20");
        
        t1.setCompany(c2);
        t2.setCompany(c1);
        t3.setCompany(c3);
        
        p1.signContract(t1);
        p1.signContract(t2);
        p1.signContract(t3);
        
        System.out.println("Duració contracte 2: " + t2.duration() + " dies");
        
        
        System.out.println("Temps treballat de tots els contractes: " + p1.workedTime() + " dies");
        System.out.println("Número de companyies en les que ha treballat p1: " + p1.nCompanies());
    } 
   
}