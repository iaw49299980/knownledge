/*
 * Company.java
 * 
 * Copyright 2010 Pablo Hernando <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.HashSet;

/**
 * Modelizes a Company.
 */
public class Company {
    
    private int cif;
    private String name;
    private String sector;
    
    /**
     * Constructor.
     */
    public Company() {
       // this.visitDateTime = JodaDT.formatDDMMYYYYhh(LocalDateTime.now());
    }
    public Company(int cif) {
        this.cif = cif;
    }
    public Company(int cif, String name, String sector) {
        this.cif = cif;
        this.name = name;
        this.sector = sector;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + cif;
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Company other = (Company) obj;
        if (cif != other.cif)
            return false;
        return true;
    }
    /**
     * Getters y setters
     */
    public int getCif() {
        return this.cif;
    }
}