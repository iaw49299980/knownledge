/*
 * Contract.java
 * 
 * Copyright 2010 Pablo Hernando <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.time.LocalDateTime;
import java.util.HashSet;

/**
 * Modelizes a Flight.
 */
public class Flight {
    
    private Sting code;
    private String departureDT;
    private String departureAirport;
    private String arrivalAirport;
    private Plane plane;
    
    /**
     * Constructor.
     */
    public Flight() {
        //this.visitDateTime = JodaDT.formatDDMMYYYYhh(LocalDateTime.now());
    }
    public Flight(String code) {
        this.code = code;
    }
    public Flight (String code, String departureDT) {
        this.code = code;
        this.initialDate = initialDate;
        this.endDate = endDate;
    }
    public int durationInHours() {
        
    }
    public String duration() {
        
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + code;
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Flight other = (Flight) obj;
        if (code == null) {
            if (other.code != null)
                return false;
        } else if (!code.equals(other.code))
            return false;
        return true;
    }
    /**
     * Getters y setters
     */
    public Company getCompany() {
        return this.company;
    }
    public String getInitialDate() {
        return this.initialDate;
    }
    public String getEndDate() {
        return this.endDate;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
}