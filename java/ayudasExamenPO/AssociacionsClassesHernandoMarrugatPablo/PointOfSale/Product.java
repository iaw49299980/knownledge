/*
 * Plane.java
 * 
 * Copyright 2010 Pablo Hernando <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


/**
 * Modelizes a Product.
 */
public class Product {
    
    private int ean;
    private String description;
    private String brand;
    private double price;
    
    
    /**
     * Constructors.
     */
    public Product() {
        this.ean = 123;
    }
    public Product(int ean) {
        this.ean = ean;
    }
    public Product(int ean, String description, String brand, double price) {
        this.ean = ean;
        this.description = description;
        this.brand = brand;
        this.price = price;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ean;
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Product other = (Product) obj;
        if (ean != other.ean)
            return false;
        return true;
    }
    /*
     * Setters i getters 
     *
     */
    public double getPrice() {
        return this.price;
    }
}