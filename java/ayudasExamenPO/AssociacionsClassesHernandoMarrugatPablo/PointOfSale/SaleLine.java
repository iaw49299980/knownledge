/*
 * SaleLine.java
 * 
 * Copyright 2010 Pablo Hernando <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.HashSet;

/**
 * Modelizes a SaleLine.
 */
public class SaleLine {
    
    private int nup;
    private Product product;
    
    
    /**
     * Constructors.
     */
    public SaleLine() {
        this.nup = 123;
    }
    public SaleLine(int nup, Product product) {
        this.nup = nup;
        this.product = product;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + nup;
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SaleLine other = (SaleLine) obj;
        if (nup != other.nup)
            return false;
        return true;
    }
    /*
     * Setters i getters
     *
     */ 
    public void setProduct(Product product) {
        this.product = product;
    }
    public int getNup() {
        return this.nup;
    }
    public Product getProduct() {
        return this.product;
    }
}
