/*
 * ProductTUI.java
 * 
 *
 */


public class ProductTUI {
    public static void main(String[] args) {
        // Creeem 3 productes
        Product p1 = new Product(1, "", "", 5);
        Product p2 = new Product(2, "", "", 6);
        Product p3 = new Product(3);
        // Creem un cataleg que contindrà productes
        Catalog c1 = new Catalog("Pei");
        // Creem 2 lineas de venta amb els productes p1 i p2
        SaleLine sl1 = new SaleLine(1, p1);
        SaleLine sl2 = new SaleLine(2, p2);
        // Creem un ticket de venta que contindrà lineas de venta
        Sale s = new Sale();
        // Afegim la linea de venta 1 i 2 al ticket
        s.add(sl1);
        s.add(sl2);
        /** SALE **/
        // Comprovem el total del ticket
        System.out.println(s.totalAmount());
        // Esborrem la linea de venta 1
        System.out.println(s.remove(sl1));
        // Tornem a comprovar el total del preu del ticket
        System.out.println(s.totalAmount());
        /** CATÀLEG **/
        // Afegim un producte al cataleg
        System.out.println(c1.add(p1));
        // Comprovat que donat un int que representa el codi de un producte, el busqui dins del catàleg
        System.out.println(c1.search(1));
        // Esborrem un producte
        System.out.println(c1.remove(p1));
    } 
   
}