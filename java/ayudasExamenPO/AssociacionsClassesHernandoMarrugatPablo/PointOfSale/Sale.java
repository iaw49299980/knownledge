/*
 * SaleLine.java
 * 
 * Copyright 2010 Pablo Hernando <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


import java.util.HashSet;
import java.time.LocalDateTime;

/**
 * Modelizes a SaleLine.
 */
public class Sale {
    
    private int code;
    private String datetime;
    private HashSet<SaleLine> saleLines;
    
    /**
     * Constructors.
     */
    public Sale() {
        this.code = 13;
        saleLines = new HashSet<SaleLine>();
        datetime = JodaDT.formatDDMMYYYYhhmmss(LocalDateTime.now());
    }
    public Sale(int code, String datetime) {
        this.code = code;
        this.datetime = datetime;
        saleLines = new HashSet<SaleLine>();
    }
    /**
     * Afegeix una linea de venta
     * 
     * return true || false
     */
    public boolean add(SaleLine sl) {
        return saleLines.add(sl);        
    }
    /**
     * Esborra una linea de venta
     * 
     * return true || false
     */
    public boolean remove(SaleLine sl) {
        return saleLines.remove(sl);        
    }
    /**
     * Calcula el preu total de les lineas de venta que conté el tiket
     * 
     * return preu
     */
    
    public double totalAmount() {
        double sumatori = 0;
        for (SaleLine sl : this.saleLines) {
            sumatori += sl.getProduct().getPrice();
        }
        return sumatori;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + code;
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Sale other = (Sale) obj;
        if (code != other.code)
            return false;
        return true;
    }
}