/*
 * Catalog.java
 * 
 * Copyright 2010 Pablo Hernando <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


import java.util.HashSet;

/**
 * Modelizes a Catalog.
 */
public class Catalog {
    
    private String name;
    private HashSet<Product> products;
    
    /**
     * Constructors.
     */
    public Catalog() {
        products = new HashSet<Product>();
    }
    public Catalog(String name) {
        this.name = name;
        products = new HashSet<Product>();
    }
    /**
     * Afegeix un producte
     * 
     * return true || false
     */
    public boolean add(Product product) {
        return products.add(product);        
    }
    /**
     * Esborra un producte
     * 
     * return true || false
     */
    public boolean remove(Product product) {
        return products.remove(product);        
    }
    /**
     * Donat un codi d'un producte el busca dins del cataleg
     * 
     * @param int codi
     * return true || false
     */
    public boolean search(int ean) {
        Product product = new Product(ean);
        return products.contains(product);
    }
    /**
     * return numero de productes dins del catàleg
     */
    public int productNumber() {
        return products.size();
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + name.hashCode();
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Catalog other = (Catalog) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}