/*
 * VisitTUI.java
 * 
 *
 */


import java.util.HashSet;

public class LorryTUI {

    public static void main(String[] args) {
        
        Lorry l1 = new Lorry("1", 25, 4);
        Lorry l2 = new Lorry("2", 27, 3);
        Lorry l3 = new Lorry("2", 27, 1);
        Ferry f1 = new Ferry("1", "Pei", "pearl harbour", 5, 4, 62);
        
        System.out.println(f1.board(l1));
        f1.board(l2);
        System.out.println(f1.board(l3));
        System.out.println(f1.isBoarded(l1));
        System.out.println(f1.lorryInPosition(1));
        System.out.println(f1.tollPrice(l1));
        System.out.println(f1.tollPrice(l2));
        System.out.println(f1.collectedTolls());
        /*
        System.out.println(s1.countVisits("19/03/2019-21:19", "21/03/2019-20:19"));
        System.out.println(s1.countVisits());
        System.out.println(s1.cancel(v1));
        System.out.println(s1.countVisits());
        s1.cancelAll();
        System.out.println(s1.countVisits());*/
    } 
   
}