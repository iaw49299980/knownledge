/*
 * Visit.java
 * 
 * Copyright 2010 Pablo Hernando <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


/**
 * Modelizes a Lorry.
 */
public class Lorry {
    
    private String regNumber;
    private double weight;
    private int nAxies;
    
    /**
     * Constructor.
     */
    public Lorry() {
        
    }
    public Lorry(String regNumber) {
        this.regNumber = regNumber;
    }
    public Lorry(String regNumber, double weight, int nAxies) {
        this.regNumber = regNumber;
        this.weight = weight;
        this.nAxies = nAxies;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((regNumber == null) ? 0 : regNumber.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Lorry other = (Lorry) obj;
        if (regNumber == null) {
            if (other.regNumber != null)
                return false;
        } else if (!regNumber.equals(other.regNumber))
            return false;
        return true;
    }
    /**
     * Getters y setters
     */
    public int getNAxies() {
        return this.nAxies;
    }

    public double getWeight() {
        return this.weight;
    }
}