/*
 * Surgery.java
 * 
 * Copyright 2010 Pablo Hernando <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.ArrayList; 

/**
 * Modelizes a Ferry.
 */
public class Ferry {
    
    private String regNumber;
    private String name;
    private String harbourName;
    private double pricePerAxie;
    private double pricePerTon;
    private double maxWeight;
    // Almatzenem totes les visites
    private ArrayList<Lorry> boardedLorries;
    
    /**
     * Constructor.
     */
    public Ferry() {
        this.boardedLorries = new ArrayList<Lorry>();
    }
    public Ferry(String regNumber) {
        this.regNumber = regNumber;
        this.boardedLorries = new ArrayList<Lorry>();
    }
    public Ferry(String regNumber, String name, String harbourName, 
                 double pricePerAxie, double pricePerTon, double maxWeight) {
        this.regNumber = regNumber;
        this.name = name;
        this.harbourName = harbourName;
        this.pricePerAxie = pricePerAxie;
        this.pricePerTon = pricePerTon;
        this.maxWeight = maxWeight;
        this.boardedLorries = new ArrayList<Lorry>();
    }
    public double totalWeight() {
        double sumatori = 0;
        int size = boardedLorries.size();
        Lorry actual;
        for (int i = 0; i < size; i++) {
            actual = boardedLorries.get(i);
            sumatori += actual.getWeight();
        }
        return sumatori;
    }
    /**
     * Comprova si un lorry està embarcat.
     * 
     * @return true || false
     */
    public boolean isBoarded (Lorry lorry) {
        boolean is = this.boardedLorries.contains(lorry);
        return is;
    }
    /**
     * Comprova si un lorry es pot embarcar.
     * 
     * @return true || false
     */
    public boolean canBoarded (Lorry lorry) {
        if (totalWeight() + lorry.getWeight() < maxWeight) {
            return true;
        }
        return false;
    }
    /**
     * Embarca un lorry.
     * 
     * @return true || false
     */
    public boolean board (Lorry lorry) {
        if (canBoarded(lorry)) {
            return this.boardedLorries.add(lorry);
        }
        return false;
    }
    /**
     * Lorry en una certa posicio.
     * 
     * 
     * @return Lorry
     */
    public Lorry lorryInPosition (int n) {
        return boardedLorries.get(n);
    }
    /**
     * Calcula la taxa que ha de pagar un camió
     * 
     * @return price
     */
    public double tollPrice(Lorry lorry) {
        return (lorry.getWeight() * pricePerTon) + (lorry.getNAxies() * pricePerAxie);
    }
    /**
     * Calcula el total recaudat.
     * 
     * @return total recaudat
     */
    public double collectedTolls() {
        double sumatori = 0;
        int size = boardedLorries.size();
        Lorry actual;
        for (int i = 0; i < size; i++) {
            actual = boardedLorries.get(i);
            sumatori += tollPrice(actual);
        } 
        return sumatori;
    }
    
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((regNumber == null) ? 0 : regNumber.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Ferry other = (Ferry) obj;
        if (regNumber == null) {
            if (other.regNumber != null)
                return false;
        } else if (!regNumber.equals(other.regNumber))
            return false;
        return true;
    }
}