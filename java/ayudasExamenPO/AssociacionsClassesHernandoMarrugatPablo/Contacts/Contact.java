/*
 * Person.java
 * 
 * Copyright 2010 Pablo Hernando <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


import java.time.LocalDateTime;
import java.util.HashSet;

/**
 * Modelizes a the contacts of a person.
 */
public class Contact {
    
    private int dni;
    private String name;
    private String phone;
    
    // Almatzenem tots els contactes
    private HashSet<Contact> contacts;
    
    /**
     * Constructor.
     */
    public Contact() {
        this.contacts = new HashSet<Contact>();
    }
    public Contact(int dni) {
        this.dni= dni;
        this.contacts = new HashSet<Contact>();
    }
    public Contact(int dni, String name, String phone) {
        this.dni= dni;
        this.name= name;
        this.phone= phone;
        this.contacts = new HashSet<Contact>();
    }
    /**
     * 
     * Busquem un contacte
     * 
     */
    public boolean search (int dni) {
        for (Contact c : this.contacts) {
            if (c.getDni() == dni) {
                return true;
            }
        }
        return false;
    }
    /**
     * Afegim un contacte
     * 
     */
    public boolean addContact (Contact contact) {
        return this.contacts.add(contact);
    }
    /**
     * Borrem un contacte
     * 
     */
    public boolean removeContact (Contact contact) {
        return this.contacts.remove(contact);
    }
    /**
     * Borrem tots els contactes
     * 
     */
    public void removeAll (Contact contact) {
        this.contacts.clear();
    }
    /**
     * Quants contactes
     * 
     */
    public void listContacts () {
        for (Contact c : this.contacts) {
            System.out.printf(c.getDni() + "\t" + c.getName() + "\t" + c.getPhone() + "\n");
        }
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + dni;
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Contact other = (Contact) obj;
        if (dni != other.dni)
            return false;
        return true;
    }
    public int getDni() {
        return dni;
    }
    public String getPhone() {
        return phone;
    }
    public String getName() {
        return name;
    }
}