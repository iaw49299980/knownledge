/*
 * Person.java
 * 
 * Copyright 2010 Pablo Hernando <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

/**
 * Modelizes a the contacts of a person.
 */
public class ContactTUI {
    public static void main(String[] args) {
        
        Contact c1 = new Contact(49299980);
        Contact c2 = new Contact(49299968, "Pablo", "615327584");
        Contact c3 = new Contact(49299979);
        
        
        System.out.println(c1.addContact(c2));
        c1.listContacts();
        System.out.println(c1.search(c2.getDni()));
        System.out.println(c1.addContact(c3));
        c1.listContacts();
        
        
       /* System.out.println("Duració contracte 2: " + t2.duration() + " dies");
        
        
        System.out.println("Temps treballat de tots els contractes: " + p1.workedTime() + " dies");
        System.out.println("Número de companyies en les que ha treballat p1: " + p1.nCompanies());*/
    } 
}