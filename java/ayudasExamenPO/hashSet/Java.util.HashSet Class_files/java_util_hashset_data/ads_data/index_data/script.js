// JavaScript Document
//HTML5 Ad Template JS from DoubleClick by Google

var container;
var content;
// var bgExit;
// var ctaExit;

 var timeReset =         17000; //0 => no bucle
// var timeReset =         0; //0 => no bucle
var intval="";
/////////ANIMATION FUNCTIONS
function startAnimation() {
    "use strict";
    var elements =  document.getElementsByClassName('animado'),
        i;
    for (i = 0; i < elements.length; i = i + 1) {
        elements[i].style.animationPlayState =          'running';
        elements[i].style.webkitAnimationPlayState =    'running';
        elements[i].style.MozAnimationPlayState =       'running';
    }
   if (timeReset > 0) {
       intval= setTimeout(function () {
            stopAnimation();
        }, timeReset);
    }
}

function stopAnimation() {
    "use strict";
    var elements =  document.getElementsByClassName('animado'),
        i,
        elm,
        newone;

    for (i = 0; i < elements.length; i = i + 1) {
        elm = elements[i];
        newone = elm.cloneNode(true);

        elm.parentNode.replaceChild(newone, elm);
    }
    startAnimation();
}
//////////////END ANIMATION FUNCTION

//Function confirm if the creative is visible
enablerInitHandler = function(e) {
	if(Enabler.isVisible()) {
	startAd();
	} else {
	Enabler.addEventListener(studio.events.StudioEvent.VISIBLE, startAd);
	}
};

//Start the creative
startAd = function(e) {
	
	startAnimation();
};


//Add Event Listeners
addListeners = function(e) {
	// bgExit.addEventListener('touchEnd', bgExitHandler, false);
	// bgExit.addEventListener('click', bgExitHandler, false);
};

function clickthrough() {
    "use strict";
    Enabler.exit("VER_MAS");
		clearInterval(intval);
	stopAnimation();
}


//Wait for the content to load to call the start od the ad
window.onload = function(){
		if (Enabler.isInitialized()) {
	  enablerInitHandler();
	} else {
	  Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
	}
		
};
