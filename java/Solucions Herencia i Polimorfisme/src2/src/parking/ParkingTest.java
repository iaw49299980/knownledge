/*
 * ParkingTest.java        1.0 14/05/2013
 *
 * Models the program.
 *
 * Copyright 2013 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

package parking;

public class ParkingTest {

    public static void main(String[] args) {
        Parking p = new Parking(10);
        Car c1 = new Car("123ABC", 125, 5);
        Car c2 = new Car("123ABD", 125, 5);
        Car c3 = new Car("123ABE", 116, 2);
        Car c4 = new Car("123ABF", 116, 4);
        Motorbike m1 = new Motorbike("321ABC", 125);
        Motorbike m2 = new Motorbike("321ABD", 125);
        p.add(c1, 1);
        p.add(c2, 4);
        p.add(c3, 2);
        p.add(c4, 2);
        p.add(m1, 5);
        p.add(m1, 8);
        p.add(m2, 10);
        
        System.out.println(p);
        System.out.println(p.calculateTotal());        

    }

}
