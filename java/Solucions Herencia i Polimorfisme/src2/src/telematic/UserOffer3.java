/*
 * UsuariOferta3.java
 * 		
 * Copyright 2011 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

package telematic;

public class UserOffer3 extends UserWithOffer {

    // Constructor
    public UserOffer3(String name, String dni, double priceMinute) {
        super(name, dni, priceMinute);
    }

    @Override
    public double billAmount() {
        // Look for the longest connection
        double minutesLongestConection = 0;
        for (Connection c : this.connections) {
            double minutes = c.nMinutes();
            if (minutes > minutesLongestConection) {
                minutesLongestConection = minutes;
            }
        }
        // Subtract the price of the longest connection
        return this.totalMinutes() * this.priceMinute - minutesLongestConection * this.priceMinute;
    }

}
