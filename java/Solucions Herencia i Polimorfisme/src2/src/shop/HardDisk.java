/*
 * HardDisk.java
 * 
 * Copyright 2010 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

package shop;

/**
 * Modelizes a hard disk.
 */
public class HardDisk extends Product {

    /** La capacitat del disc dur */
    private float capacity;

    // Constructor

    public HardDisk(String code, String description, int units, float price, float capacity) {
        super(code, description, units, price);
        this.capacity = capacity;
    }

    @Override
    public float price() {
        return this.getBasePrice() * this.capacity * (float) 0.9;
    }

    @Override
    public String toString() {
        return this.getCode() + "\t" + this.getDescription() + "\t" + this.getUnits()
            + "\t" + this.capacity + "GB" + "\t\t" + this.price()
            + "\t" + this.price() * this.getUnits();
    }

    // Getters & setters

    public float getCapacity() {
        return this.capacity;
    }

}
