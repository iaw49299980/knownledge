/*
 * Chronometer.java 1.1 20/02/2012 
 *
 * Copyright 2010-2012 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *                     Joaquim Laplana Tarragona <jlaplana@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

package chronometer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Modelizes a chronometer.
 */
public final class Chronometer {

    /** Initial time */
    private LocalDateTime t1;
    /** Final time */
    private LocalDateTime t2;

    /**
     * Starts chronometer.
     */
    public void start() {
        this.t1 = LocalDateTime.now(); 
    }

    /**
     * Stops chronometer.
     */
    public void stop() {
        this.t2 = LocalDateTime.now();
    }

    /**
     * Resets chronometer.
     */
    public void reset() {
        this.start();
        this.t2 = null;
    }

    /**
     * Shows interval time between t1 and t2.
     * 
     * @return time interval with format hh:mm:ss
     */
    public String getTimeInterval() {
        if(this.t1 != null && this.t2 != null){ 
           long seconds = JodaDT.durationInSeconds(t1, t2);;
           long hours = seconds / 3600;
           seconds = seconds - (hours * 3600);
           long minutes = seconds / 60;
           seconds = seconds - (minutes * 60);
           return hours + ":" + minutes + ":" + seconds;
        }
        return "";
    }

}
