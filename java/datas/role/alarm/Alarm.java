/*
 * Alarm.java        1.1 28/11/2012 
 *
 * Copyright 2010-2012 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *                     Joaquim Laplana Tarragona <jlaplana@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

package alarm;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.Period;

/**
 * Modelizes an alarm.
 */
public class Alarm {

    /** Current datetime */
    private LocalDateTime now;
    /** Alarm's datetime */
    private LocalDateTime alarm;

    // Constructor
    public Alarm() {
        // Set current datetime
        this.now = LocalDateTime.now();
        // Deactivate the alarm (past datetime)
        this.alarm = this.now.minusDays(1);
    }

    // Methods

    /**
     * Sets alarm.
     * 
     * @param day alarm's day
     * @param month alarm's month
     * @param year alarm's year
     * @param hour alarm's hour
     * @param minute alarm's minute
     */
    public void setAlarmDateTime(int day, int month, int year, int hour, int minute) {
        this.alarm = LocalDateTime.of(year, month, day, hour, minute); 
    }
    
     /**
     * Sets alarm.
     * 
     * @param date with format DD/MM/YYYY-hh:mm:ss
     */
    public void setAlarmDateTime(String date) {
        this.alarm = JodaDT.parseDDMMYYYYhhmmss(date); 
    } 
       
    /**
     * Generates a string with datetime's alarm (format DD/MM/YYYY-hh:mm).
     * 
     * @return cadena amb el temps de l'alarma
     */
    public String getAlarmDatetime() {
        // Get alarm's year
        int year = this.alarm.getYear();
        // Get alarm's month
        int month = this.alarm.getMonthValue();
        // Get alarm's day
        int day = this.alarm.getDayOfMonth();
        // Get alarm's hour
        int hour = this.alarm.getHour();
        // Get alarm's minute
        int minute = this.alarm.getMinute();
        // Build formatted string        
        return day + "/" + month + "/" + year + "-" + hour + ":" + minute;
        // Or using JodaDT:
        
        // return JodaDT.formatDDMMAAAAhhmm(this.alarm);
    }

    /**
     * Calculates how many seconds are left to alarm's datetime. 
     * Returns -1 if the alarm's datetime is in the past.
     * 
     * @return the number of seconds or -1
     */
    public long secondsLeft() {
        // Sets now with current datetime
        this.now = LocalDateTime.now();
        // Gets duration in seconds between now and alarm's datetime
        long t = JodaDT.durationInSeconds(now, alarm);
        if (t < 0) {
            t = -1;
        }
        return t;
    }

    /**
     * Generates a string with the time left to alarm's datetime. 
     * Returns null if the alarm's datetime is in the past.
     * 
     * @return a string
     */
    public String timeLeft() {
        String tl = null;
        // Set current datetime
        this.now = LocalDateTime.now();
        if ( this.alarm.isAfter(this.now) ) {
           Period p = Period.between(this.now.toLocalDate(),this.alarm.toLocalDate());
           LocalDateTime d = LocalDateTime.of(alarm.getYear(),alarm.getMonthValue(),
               alarm.getDayOfMonth(),this.now.getHour(),this.now.getMinute(),this.now.getSecond());
           long seconds = JodaDT.durationInSeconds(d, alarm);;
           long alarmHour = Math.abs(seconds) / 3600;
           seconds = Math.abs(seconds) - (alarmHour * 3600);
           long alarmMinutes = seconds / 60;
           long alarmSeconds = seconds - (alarmMinutes * 60);
            tl = p.getYears() + " anys, " + p.getMonths() + " mesos, " + p.getDays() + " dies, " +
                 alarmHour + " hores, " + alarmMinutes + " minuts, " + alarmSeconds + " segons.";
                    
        }
        return tl;
    }

    /**
     * Determines if the alarm is ringing.
     * 
     * @return true if alrm is ringing, false otherwise
     */
    public boolean isRinging() {
        // Set current datetime
        this.now = LocalDateTime.now();
        // If alarm's datetime is now (don't control seconds), alarm is ringing
        boolean ring = alarm.getYear() == now.getYear()
                && alarm.getMonthValue() == now.getMonthValue()
                && alarm.getDayOfMonth() == now.getDayOfMonth()
                && now.getHour() == alarm.getHour()
                && now.getMinute() == alarm.getMinute();
        return ring;
    }

}
