package alarm;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.Period;

/**
 * Modelizes a calendar to know the fertility periods of a woman.
 * 
 * For more information, 
 * see http://en.wikipedia.org/wiki/Calendar-based_contraceptive_methods
 * 
 * @author Mònica Ramírez Arceda
 * @version 22/02/2012
 */
public class AlamrTUI {
  
      public static void main(String[] args) {
        Alarm f = new Alarm();
        f.setAlarmDateTime("26/03/2019-18:50:00");
        System.out.println ("Nombre de segons " + f.secondsLeft());
        System.out.println ("Falten " + f.timeLeft());
      }
}