/*
 * Accident.java
 * 		
 * Copyright 2011 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

 

public class Accident {

	/** Identificador de l'accident. */
	private long id;
	/** Data de l'accident en format DD/MM/AAAA */
	private String data;
	/** Lloc de l'acccident */
	private String lloc;
	/** El cost de l'acccident pagat per l'empresa. */
	private double cost;
	/** Comptador d'accidents enregistrats */
	private static long compatdorAccidents = 0;

	/**
	 * Constructor.
	 * 
	 * @param data Data de l'accident en format DD/MM/AAAA
	 * @param lloc Lloc de l'acccident.
	 */
	public Accident(String data, String lloc) {
		this.data = data;
		this.lloc = lloc;
		this.cost = 0;
		this.id = Accident.compatdorAccidents;
		Accident.compatdorAccidents++;
	}

	@Override
	public String toString() {
		return this.id + "\t" + this.data + "\t" + this.lloc + "\t" + this.cost;
	}

	// Getters i setters.

	public String getData() {
		return data;
	}

	public String getLloc() {
		return lloc;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public long getId() {
		return id;
	}

	public double getCost() {
		return this.cost;
	}
}
