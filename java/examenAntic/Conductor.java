/*
 * Conductor.java
 *   
 * Copyright 2019 Carlos Esteban <carlosbcn169@gmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

 

import java.time.LocalDateTime;


public class Conductor {
 /** El DNI del conductor */
 private String dni;
 /** El nom del conductor. */
 private String nom;
 /** La data de naixement del conductor */
 private String dataNaixement;

 /**
  * Constructor.
  * 
  * @param dni El DNI del conductor
  * @param nom El nom del conductor.
  * @param dataNaixement La data de naixement del conductor.
  */
 public Conductor(String dni, String nom, String dataNaixement) {
  this.dni = dni;
  this.nom = nom;
  this.dataNaixement = dataNaixement;
 }

 /**
  * Calcula quants anys té el conductor en la data passada per paràmetre. Si la data és anterior
  * a
  * la data de naixement retorna 0.
  * 
  * @param data Data en format DD/MM/AAAA.
  * @return L'edat del conductor en anys o 0.
  */
 public int edat(String data) {
  LocalDateTime naixement = JodaDT.parseDDMMYYYY(data);
  LocalDateTime dataActual = LocalDateTime.now();
  long totalSegons = JodaDT.durationInSeconds(naixement, dataActual);
  long anys = totalSegons / 3600*24*365;
  return (int) anys;
 }

 // Getters i setters.

 public String getDni() {
  return dni;
 }

 public String getNom() {
  return nom;
 }

}
