/*
 * Tercers.java
 *   
 * Copyright 2019 Carlos Esteban Casado <carlosbcn169@gmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

public class Tercers extends Polissa {
    //Quota polissa Turisme
    private static final double QUOTA_TURISME = 400.0;
    //Quota polissa monovolúm
    private static final double QUOTA_MONOVOLUM = 800.0;
    
    //Constructor
    public Tercers(String dataContractacio) {
        super(dataContractacio);
    }
    
    @Override
    public double preuBaseSegonsPolissa() {
        double preu = 0;
        /*
         *  TURISME + TERCERS = 400
         *  MONOVOLUM + TERCERS = 800
         */   
        if (this.cotxe instanceof Turisme) {
            preu = QUOTA_TURISME;
        } else if (this.cotxe instanceof Monovolum) {
            preu = QUOTA_MONOVOLUM;
        }
        return preu;
    }
    
    @Override
    public String toString() {
        return "Polissa Tercers: Preu base " + this.preuBaseSegonsPolissa();
    }
}