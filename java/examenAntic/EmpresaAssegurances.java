/*
 * EmpresaAssegurances.java
 * 		
 * Copyright 2011 NOM I COGNOMS <MAIL>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

 

import java.util.ArrayList;

import library.utilitats.JodaDT;

import org.joda.time.DateTime;

public class EmpresaAssegurances {

	/** Conjunt de pòlisses de l'empresa */
	private ArrayList<Polissa> polisses;

	/**
	 * Constructor per defecte.
	 */
	public EmpresaAssegurances() {
		polisses = new ArrayList<Polissa>();
	}

	/**
	 * Afegeix una pòlissa a l'empresa.
	 * 
	 * @param p La pòlissa a afegir.
	 * @return La polissa aefgida si s'ha pogut afegir, null altrament.
	 */
	public Polissa afegeixPolissa(Polissa p) {
		boolean afegit = polisses.add(p);
		if (afegit) {
			return p;
		}
		return null;
	}

	/**
	 * A partir de les dades passades per paràmetre, s'afegeix una pòlissa a l'empresa a data
	 * actual.
	 * 
	 * La pòlissa que afegim ha de tenir el conductor, el cotxe i el preu adients segons les dades
	 * que passem per paràmetre..
	 * 
	 * @param dni El dni del conductor.
	 * @param nom El nom del conductor.
	 * @param dataNaixement La data de naixement del conductor.
	 * @param matricula La matrícula del cotxe.
	 * @param marca La marca del cotxe.
	 * @param model El model del cotxe.
	 * @param tipusCotxe El tipus de cotxe: "turisme" o "monovolum"
	 * @param tipusPolissa El tipus de pòlissa que es vol contractar: "tercers" o "totrisc"
	 * @param coberturaEstranger Si es desitja o no tenir cobertura a l'estranger. Si l'assegurança
	 *        és "tercers" aquest paràmtre serà sempre false.
	 * @return La pòlissa afegida si s'ha pogut afegir, null altrament.
	 */
	public Polissa afegeixPolissa(String dni, String nom, String dataNaixement, String matricula,
		String marca, String model, String tipusCotxe, String tipusPolissa,
		boolean coberturaEstranger) {
		// TODO
		return null;
	}

	/**
	 * Recorre totes les pòlisses de l'empresa i renova aquelles que es poden renovar.
	 * 
	 * @return El nombre de pòlisses que s'han pogut renovar.
	 */
	public int renovaPolisses() {
		// TODO
		return 0;
	}

	/**
	 * Llista totes aquelles pòlisses que no han estat vençudes.
	 * Disposeu del format desitjat a l'enunciat de l'examen.
	 */
	public void llistaPolissesNoVencudes() {
		// TODO
	}

	/**
	 * Llista tots els accidents corresponent a pòlisses no vençudes i mostra el total pagat per
	 * l'empresa.
	 * Disposeu del format desitjat a l'enunciat de l'examen.
	 */
	public void llistaAccidents() {
		// TODO
	}
}
