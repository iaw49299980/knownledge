/*
 * TotRisc.java
 *   
 * Copyright 2019 Carlos Esteban Casado <carlosbcn169@gmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

public class TotRisc extends Polissa {
    //Quota polissa Turisme
    private static final double QUOTA_TURISME = 1000.0;
    //Quota polissa monovolúm
    private static final double QUOTA_MONOVOLUM = 3000.0;
    //Quota cobertura extranger
    private static final double QUOTA_ESTRANGER = 100;
    
    //Constructor
    public TotRisc(String dataContractacio) {
        super(dataContractacio);
    }
    
    @Override
    public double preuBaseSegonsPolissa() {
        
        /*
         *  TURISME + TotRisc = 1000
         *  MONOVOLUM + TotRisc = 3000
         */   
        if (this.cotxe instanceof Turisme) {
            return QUOTA_TURISME + 100 * QUOTA_ESTRANGER;
        } else {
            return QUOTA_MONOVOLUM + 200 * QUOTA_ESTRANGER;
        }
    }
    
    @Override
    public String toString() {
        return "Polissa Tot Risc: Preu base " + this.preuBaseSegonsPolissa();
    }
}