/*
 * Program.java        1.0 08/01/2012
 *
 * Models the program.
 *
 * Copyright 2011 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *                Joaquim Laplana Tarragona <jlaplana@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Calculates the first position where two strings differs.
     * 
     * @param str1 a string.
     * @param str2 a string.
     * @return the position.
     */
    public int positionDiffers(String str1, String str2) {
        int i = 0;
        boolean equals = true;
        int length1 = str1.length();
        int length2 = str2.length();
        int maxLength = length1 < length2 ? length1 : length2;
        while (i < maxLength && equals) {
            equals = str1.charAt(i) == str2.charAt(i);
            i++;
        }
        if (equals && length1 == length2) {
            // str1 and str2 are equals
            return -1;
        } else if (equals && length1 != length2) {
            // str1 and str2 starts in the same way, but one is longer than the
            // other
            return maxLength;
        } else {
            // srt1 and str2 differs in some position
            return i - 1;
        }
    }
}
