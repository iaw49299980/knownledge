/*
 * Program.java        1.0 08/01/2012
 *
 * Models the program.
 *
 * Copyright 2011 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *                Joaquim Laplana Tarragona <jlaplana@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program2 {

    /**
     * Determines if all the words of a string end with the same character.
     * 
     * @param s a string
     * @return true if all the words ens with the same character, false otherwise.
     */
    public boolean sameEndingChar(String s) {
        // Get words in an array
        s = s.trim();
        String[] words = s.split("\\s+");
        // Get the last character of the first word
        char lc = words[0].charAt(words[0].length() - 1);
        int i = 1;
        boolean sec = true;
        while (i < words.length && sec) {
            // Check if the last char of each word is the same to the last char of the first word
            sec = words[i].charAt(words[i].length() - 1) == lc;
            i++;
        }
        return sec;
    }
}
