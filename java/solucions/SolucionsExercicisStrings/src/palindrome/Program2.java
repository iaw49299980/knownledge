/*
 * Program2.java        1.0 17/01/2013
 *
 * Models the program.
 *
 * Copyright 2013 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program2 {

    /**
     * Remove all blank spaces of a string.
     * 
     * @param str a string.
     * @return the string without blank spaces.
     */
    public String removeSpaces(String str) {
        return str.replace(" ", "");
    }

    /**
     * Reverses a string.
     * 
     * @param str a string
     * @return str reversed.
     */
    public String reverse(String str) {
        String strRev = "";
        for (int i = 0; i < str.length(); i++) {
            // Put the character before the new reversed string
            strRev = str.charAt(i) + strRev;
        }
        return strRev;
    }

    /**
     * Determines whether a string is a palindrome.
     * 
     * @param s a string
     * @return true if s is a palindrome, false otherwise.
     */
    public boolean isPalindrome(String s) {
        String sWithouthSpaces = removeSpaces(s);
        String sWithouthSpacesReversed = reverse(sWithouthSpaces);        
        return sWithouthSpaces.equals(sWithouthSpacesReversed);
        //return removeSpaces(s).equals(reverse(removeSpaces(s)));
    }

}
