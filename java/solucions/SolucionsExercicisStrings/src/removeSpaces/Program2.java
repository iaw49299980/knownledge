/*
 * Program2.java        1.0 17/01/2013
 *
 * Models the program.
 *
 * Copyright 2013 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program2 {
    

    /**
     * Remove all blank spaces of a string.
     * 
     * @param str a string.
     * @return the string without blank spaces.
     */
    public String removeSpaces(String str) {
        return str.replace(" ", "");
    }

}
