/*
 * Program.java        1.0 02/11/2011
 *
 * Models the program.
 *
 * Copyright 2011 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *                Joaquim Laplana Tarragona <jlaplana@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Locale;
import java.util.Scanner;

public class ProgramTUI3Plantilla {
  
     /**
     * Calculates the absolute value of a real number.
     * 
     * @param n a real number.
     * @return the absolute value of -n-.
     */
      public double abs(double n) {
        double absoluteValue;
        // Initialize absoluteValue with the value of -n-
        absoluteValue = n;
        // If n is negative, change the sign to absoluteValue
        if (n < 0) {
            absoluteValue = -n;
        }
        // Return the absolute value
        return absoluteValue;

    }
  
    /**
     * Determines the minimum number of two real numbers.
     * 
     * @param n1 a real number.
     * @param n2 a real number.
     * @return the minimum number.
     */
    public double min2(double n1, double n2) {
        double min;
        if (n1 < n2) {
            // If n1 is smaller than n2, n1 is the minimum number
            min = n1;
        }  else {
            // If n2 is smaller or equals than n1, n2 is the minimum number
            min = n2;
        } 
        return min;
    }


      
      
    /**
     * This is a template for a generical meun.
     * 
     * @param args Not used
     */
    public static void main(String[] args) {
        int op;
        Scanner s = new Scanner(System.in);
        // Print a menu with all the options
        System.out.print("\n\nMENÚ\n\n");
        System.out.print("1. Valor absolut\n");
        System.out.print("2. Mínim de dos nombres\n");
        System.out.print("3. Sortir\n");
        // Ask for an option
        System.out.print("\nOpció ? ");
        op = s.nextInt();
        // TODO: Common actions, if necessary.
        // Depending on the chosen option, we perform the desired operation.        
        switch (op) {
        case 1:
          
            break;
        case 2:
           
            break;
        case 3:
           
            break;
        default:
            
            break;
        }
       
    }
}
