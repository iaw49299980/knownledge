package animalsAmbInterficiesPigallICompara;

public class ComparaPerNom implements Compara
{ 
  public ComparaPerNom() {
  }
  
  public boolean compara(Animal a1, Animal a2) {
    return a1.getNom().compareTo(a2.getNom()) == 0;
  }
}