package animalsAmbInterficiesPigallICompara;

public class ComparaPerEdat implements Compara
{
  public ComparaPerEdat(){
  }
  
  public boolean compara(Animal a1, Animal a2) {
    return a1.getEdat() == a2.getEdat();
  }
}