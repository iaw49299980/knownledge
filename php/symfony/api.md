# API

## Instalar

```
    composer create-project Symfony/skeleton nombre_proyecto
```

## Paquetes/Bundles

```
    composer require maker profiler orm-fixtures --dev


    composer require symfony/apache-pack
    composer require doctrine maker
    composer require firebase/php-jwt
    composer require symfony/http-foundation
    composer require symfony/serializer
    composer require symfony/validator
    composer require symfony/property-access
```

## Entidades

```
    php bin/console make:entity

    Respondemos las preguntas del wizard

    php bin/console make:entity --regenerate
```

## BBDD

```
    Configuramos el .env
    php bin/console doctrine:database:create

    php bin/console doctrine:schema:update --force --dump-sql

    doctrine:mapping:import App\Entity annotation --path=src/Entity


``` 

## Cargar Datos

```
    En src/DataFixtures/AppFixtures

    php bin/console doctrine:fixtures:load -n
```

## .htaccess

```
    En la carpeta public


<IfModule mod_rewrite.c>
    Options -MultiViews
    RewriteEngine On
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^(.*)$ index.php [QSA,L]
</IfModule>

<IfModule !mod_rewrite.c>
    <IfModule mod_alias.c>
        RedirectMatch 302 ^/$ /index.php/
    </IfModule>
</IfModule>
```

## Url's de administración
```
url/api
url/admin
```

## Servidor

```
php -S 127.0.0.1:8000 -t public
php bin/console server:run
```

## Doctrine

```
$this->getDoctrine()
```
## Repositorio

```
$this->getDoctrine()->getRepository(Entidad::class);
```