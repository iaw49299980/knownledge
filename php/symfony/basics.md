# Symfony

## Instalar

1. Instalar Composer
2. Desde la terminal situada en la carpeta que queremos hacer nuestro proyecto introducimos:
   ```
    composer create-project symfony/website-skeleton nombre-proyecto
   ```

## Conectar BBDD

1. En el archivo .env de la carpeta root del proyecto, cambiamos la linea:
   ```
   DATABASE_URL=mysql...
   ```

## Generar entidades

Las entidades se crean a partir de las tablas de la bbdd y serán nuestra forma de comunicarnos con ellas.

1. Con la terminal situada en la carpeta del proyecto:
   ```
   php bin/console doctrine:mapping:import App\\Entity annotation --path src/Entity
   ```

### Generar getters y setters para las entidades.

1. Con la terminal situada en la carpeta del proyecto:
   ```
   php bin/console make:entity --regenerate App
   ```

### Crear Controlador

1. Con la terminal situada en la carpeta del proyecto:
   ```
   php bin/console make:controller nombreControlador
   ```

### Configurar web server

1. Con la terminal situada en la carpeta del proyecto:
   ```
   composer require symfony/apache-pack
   ```
2. Añadir .htacces a la carpeta ./public

### Rutas

1. En el archivo ./config/routes.yaml
   `animal_detail: path: /animal/{id} controller: App\Controller\AnimalController::animal defaults: {id : 2}`
   {id} Es un parámetro
