<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});





/*
|--------------------------------------------------------------------------
| API Rutas de login, register...
|--------------------------------------------------------------------------
|
*/


Route::post('usuario', 	'AuthController@usuario');

Route::group(['prefix' => 'auth'], function () {
      Route::post('login', 		'AuthController@login');
      Route::post('register', 	'AuthController@signup');
});

Route::get('route', 'RouteController@getRoute');
Route::group(['middleware' => 'verificado'], function()
{
      Route::get('logout', 		'AuthController@logout');
      Route::get('user', 		'AuthController@usuario');
});
