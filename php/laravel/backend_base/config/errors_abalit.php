<?php


return [


        /*
        |--------------------------------------------------------------------------
        | RESERVES ESPECIALS 👨🏻‍💻
        |--------------------------------------------------------------------------
        |
        */
        "0"     =>    "ERROR DESCONEGUT",
        "666"   =>    "piratilla ☠️",
        /*
        |--------------------------------------------------------------------------
        | MEDIA 700-1999
        |--------------------------------------------------------------------------
        |
        */
        // Imatges 700-899 📷
        "700"   =>    "TAMANY MÀXIM",
        "701"   =>    "NO HI HA FITXER ADJUNT",
        "702"   =>    "FORMAT D'IMATGE INCORRECTE",
        "703"   =>    "FORMAT D'IMATGE INCORRECTE",
        "704"   =>    "ERROR AL MOURE EL FITXER DE LA RUTA REMPORAL A LA FINAL",
        "705"   =>    "ERROR AL RETALLAR THUMBNAIL",
        "706"   =>    "ERROR AL COMPRIMIR",
        "707"   =>    "ERROR AL CAMBIAR DE FORMAT",
        "702"   =>    "FORMAT D'IMATGE INCORRECTE",
        // Audios 900-999🎧
        "900"   =>    "TAMANY MÀXIM",
        "901"   =>    "NO HI HA FITXER ADJUNT",
        "902"   =>    "FORMAT D'AUDIO INCORRECTE",
        "903"   =>    "ERROR AL MOURE EL FITXER DE LA RUTA TEMPORAL A LA RUTA FINAL",
        "904"   =>    "ERROR AL PUJAR EL FITXER",
        "905"   =>    "ERROR AL COMPRIMIR",
        "906"   =>    "ERROR AL CAMBAR DE FORMAT",
        // Videos 1000-1199📹
        "1000"   =>    "TAMANY MÀXIM",
        "1001"   =>    "NO HI HA FITXER ADJUNT",
        "1002"   =>    "FORMAT DE VIDEO INCORRECTE",
        "1003"   =>    "ERROR AL MOURE EL FITXER DE LA RUTA REMPORAL A LA FINAL",
        "1004"   =>    "ERROR AL PUJAR VIDEO",
        "1005"   =>    "ERROR AL COMPRIMIR",
        "1006"   =>    "ERROR AL CAMBIAR DE FORMAT",
        // fitxers 1100-1199📄
        "1101"   =>    "TAMANY MÀXIM",
        "1102"   =>    "NO HI HA FITXER ADJUNT",
        "1103"   =>    "FORMAT DE FITXER INCORRECTE",
        "1104"   =>    "ERROR AL MOURE EL FITXER DE LA RUTA REMPORAL A LA FINAL",
        "1105"   =>    "ERROR AL PUJAR EL FITXER",
        "1106"   =>    "ERROR AL COMPRIMIR",
        "1107"   =>    "ERROR AL CAMBIAR DE FORMAT",
        /*
        |--------------------------------------------------------------------------
        | USUARIS 2000-3999
        |--------------------------------------------------------------------------
        |
        */
        // Autenticacio 2000-2199
        "2001"   =>    "LOGIN INCORRECTE",
        // Registre  2200-2399
        "2201"   =>    "USERNAME JA EXISTEIX",
        "2202"   =>    "E-MAIL JA EXISTEIX",
        "2203"   =>    "PASSWORD NO COMPLEIX REQUISITS",
        "2204"   =>    "EDAT MINIMA",
        /*
        |--------------------------------------------------------------------------
        | FORMATS DE VARIABLE 4000-4999
        |--------------------------------------------------------------------------
        |
        */
        "4001"   =>    "FALTA ALGUN CAMP OBLIGATORI",
        "4002"   =>    "NIF/NIE INCORRECTE",
        "4003"   =>    "TELEFON INCORRECTE",
        "4004"   =>    "IBAN INCORRECTE",
        "4005"   =>    "INT",
        "4006"   =>    "FLOAT",
        "4007"   =>    "DOUBLE",
        "4008"   =>    "STRING",
        "4009"   =>    "ARRAY",
        "4010"   =>    "JSON",
        "4011"   =>    "TIMESTAMP",
        "4012"   =>    "DATETIME",
        "4013"   =>    "TIME",
        "4014"   =>    "BOOLEAN",
        "4015"   =>    "EMAIL",
        "4016"   =>    "VALOR MAXIM",
        "4017"   =>    "VALOR MINIM",
        "4018"   =>    "DATA FINAL ANTERIOR A L'ACTUAL",
        "4019"   =>    "HORA FINAL ANTERIOR A L'ACTUAL",
        /*
        |--------------------------------------------------------------------------
        | PAGOS 🤑 (5000-6999)
        |--------------------------------------------------------------------------
        |
        */
        // General 5000-5999
        "5001"   =>    "ERROR AL CREAR ORDRE,SUSCRIPCIO...PER PAGAR",
        "5002"   =>    "PREU MINIM INCORRECTE",
        "5003"   =>    "PREU MAXIM INCORRECTE",
        "5004"   =>    "NUMERO MINIM DE DIES PER SUSCRIPCIO",
        "5005"   =>    "DATA INSCRIPCIO ANTERIOR A ACTUAL",
        "5006"   =>    "SALDO INSUFICIENT",
        "5007"   =>    "CODI PROMOCIONAL INVÁLID",
        "5008"   =>    "AMPLIAR SUSCRIPCIO: DATA FINA ANTERIOR A LA QUE JA EXISTEIX",
        "5009"   =>    "PAGO OK",
        "5010"   =>    "PAGO KO",
        "5011"   =>    "SALDO INSUFICIENT",
        // Pay out 6000-6199
        "6001"   =>    "SALDO MÍNIM INSUFICIENT",
        "6002"   =>    "NO TENS SALDO PER COBRAR",
        "6003"   =>    "DADES BANCÁRIES INCORRECTES",
        "6004"   =>    "ERROR AL TRANSFERIR DINERS",
        /*
        |--------------------------------------------------------------------------
        | CHATS 💬 (7000-7999)
        |--------------------------------------------------------------------------
        |
        */
        "7001"   =>    "USUARI DESTÍ NO EXISTEIX",
        "7002"   =>    "EL GRUP NO EXISTEIX",
        "7003"   =>    "ERROR AL ENVIAR MISATGE",
        "7004"   =>    "L'ALTRE USUARI T'HA BLOQUEJAT",
        /*
        |--------------------------------------------------------------------------
        | SOCKETS 🌙 (8000-8999)
        |--------------------------------------------------------------------------
        |
        */
        "8001"   =>    "ERROR AL OBRIR SOCKET",
        "8002"   =>    "ERROR DE CONNEXIÓ",
        "8003"   =>    "ERROR A ENVIAR MISSATGE",
        /*
        |--------------------------------------------------------------------------
        | NOTIFICACIONS  📲(9000-9999)
        |--------------------------------------------------------------------------
        |
        */
        "9001"   =>    "ERROR AL ELIMINAR NOTIFICACIÓ",
        "9002"   =>    "ERROR AL MARCAR COM LLEGIT",
        /*
        |--------------------------------------------------------------------------
        | LIKES 👍(10000-10999)
        |--------------------------------------------------------------------------
        |
        */
        "10001"   =>    "JA HAS POSAT LIKE",
        "10002"   =>    "NO HAS POSAT LIKE",
        "10003"   =>    "L'USUARI T'HA BLOQUEJAT",
        /*
        |--------------------------------------------------------------------------
        | RATINGS ✴️(11000-11999)
        |--------------------------------------------------------------------------
        |
        */
        "11001"   =>    "JA HAS VALORAT",
        /*
        |--------------------------------------------------------------------------
        | FOLLOWS 🔀(12000-12999)
        |--------------------------------------------------------------------------
        |
        */
        "12001"   =>    "JA EL SEGUEIXES",
        "12002"   =>    "NO EL SEGUEIXES",
        "12003"   =>    "L'USUARI T'HA BLOQUEJAT",
        /*
        |--------------------------------------------------------------------------
        | COMENTARIS 💬 (13000-13999)
        |--------------------------------------------------------------------------
        |
        */
        "13001"   =>    "NO EXISTEIX EL CONTINGUT",
        "13002"   =>    "L'USUARI T'HA BLOQUEJAT",
        /*
        |--------------------------------------------------------------------------
        | ITEMS 📦(14000-16999)
        |--------------------------------------------------------------------------
        |
        */
        "14001"   =>    "ERROR AL CREAR",
        "14002"   =>    "ERROR AL REPORTAR",
        "14003"   =>    "ERROR AL ELIMINAR",
        /*
        |--------------------------------------------------------------------------
        | MAPES I RUTES 🗺(17000-19999)
        |--------------------------------------------------------------------------
        |
        */
        "17001"   =>    "NO EXISTEIX CONVINACIÓ LAT/LONG",
        "17002"   =>    "LAT/LONG EN BLANC",
        "17003"   =>    "LAT I LONG A 0.0",
        "17004"   =>    "NO ES POT CALCULAR RUTA",
        "17005"   =>    "NO S'HA POGUT BTENIR RUTA AMB LAT I LONG",
        "17006"   =>    "NO S'HA POGUT OBTENIR LAT I LONG A TRAVÉS D'ADREÇA",
        "17007"   =>    "ERROR PER API DE GOOGLE 😡",
        /*
        |--------------------------------------------------------------------------
        |CONTACTE AMB PROPIETARI DE LA APP 🙎🏼‍(20000-20999)
        |--------------------------------------------------------------------------
        |
        */
        "20001"   =>    "NO S'HA POGUT ENVIAR MAIL",
        /*
        |--------------------------------------------------------------------------
        |SINCRONITZACIÓ OFFLINE 21000-24999
        |--------------------------------------------------------------------------
        |
        */

        /*
        |--------------------------------------------------------------------------
        |GENERALS 25000-24999
        |--------------------------------------------------------------------------
        |
        */
        "25001"   =>    "ERROR AL ENVIAR EMAIL",
        "25002"   =>    "IMATGE DE SIGNATURA INCORRECTE (SI NO ES BASE64/PNG)",
        "25003"   =>    "QRs",
        "25004"   =>    "FILTRES I CERQUES",
        "25005"   =>    "CONTINGUT INNEXISTENT",
        "25006"   =>    "JA EXISTEIX EL REGISTRE A LA BD",
        /*
        |--------------------------------------------------------------------------
        |STREAMING 📹 30000-30999
        |--------------------------------------------------------------------------
        |
        */
        "30001"   =>    "ERROR AL CEAR LA SALA",
        "30002"   =>    "ERROR AL ACCEDIR A LA SALA",
        "30003"   =>    "JA NO EXISTEIX LA SALA",
        /*
        |--------------------------------------------------------------------------
        |SLOT HEAVY PER ERRORS ESPECÍFICS DE CADA APP 📱 31000 - 99999
        |--------------------------------------------------------------------------
        |
        */



















        // etc

];
