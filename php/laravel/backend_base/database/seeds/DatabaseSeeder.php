<?php

use Illuminate\Database\Seeder;
use App\user;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

    	//creación de usuario de uso diario
        $user = new user();
        $user 					= 	new user();
        $user->name 			= 	"Juan";
        $user->surnames			=	"Palomo";
        $user->email 			=	'admin@gmail.com';
        $user->password 		=	bcrypt('contra');
        // $user->phone			=	666666;
        // $user->birth_date		=	date('Y-m-d H:i:s');
        // $user->dni 				=	Str::random(10);
        // $user->id_img_profile 	=	random_int (1,999);
        // $user->id_img_dni_1 	=	random_int (1,999);
        // $user->id_img_dni_2 	=	random_int (1,999);
        // $user->direction 		=	"mi casa";

        $user->save();
        


        //creación de 100 usuarios random
        for ($i=0; $i <100 ; $i++) 
        { 
        	$user 					= 	new user();
        	$user->name 			= 	Str::random(10);
        	$user->surnames			=	Str::random(10);
        	$user->email 			=	Str::random(10).'@gmail.com';
        	$user->password 		=	bcrypt('password');
        	// $user->phone			=	666666;
        	// $user->birth_date		=	date('Y-m-d H:i:s');
        	// $user->dni 				=	Str::random(10);
        	// $user->id_img_profile 	=	random_int (1,999);
        	// $user->id_img_dni_1 	=	random_int (1,999);
        	// $user->id_img_dni_2 	=	random_int (1,999);
        	// $user->direction 		=	Str::random(10);
        	
        	$user->save();

        }

        


    }
}
