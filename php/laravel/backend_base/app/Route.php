<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    //
    protected $table = 'routes';
    public function restaurants()
    {
        return $this->belongsToMany('App\Restaurant', 'routes_restaurants', 'route_id', 'restaurant_id');
    }
}
