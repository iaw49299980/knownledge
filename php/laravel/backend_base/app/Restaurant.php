<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    //
    protected $table = 'restaurants';
    public function routes()
    {
        return $this->belongsToMany('App\Route', 'routes_restaurants', 'restaurant_id', 'route_id');
    }
    public function dishes()
    {
        return $this->hasMany('App\Dish');
    }
}
