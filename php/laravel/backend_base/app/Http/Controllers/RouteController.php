<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Route;
use App\Resturant;
use App\Dish;
use Illuminate\Support\Facades\DB;


class RouteController extends Controller
{
    //
    public function getRoute() {
        $routes = Route::with(['restaurants' => function($query) {
            $query->with('dishes');
        }])->get();
        return response()->json(["route"=>$routes]);
    }
}
