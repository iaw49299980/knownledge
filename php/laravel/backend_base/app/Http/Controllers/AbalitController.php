<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AbalitController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function saludar()
    {
        return "Hola 👋";
    }

    // Aquesta funció retorna les dades que tenim amb codi correcte 200 i el rc 1
    public function correcte($resposta = null)
    {
        return response()->json([
            'rc'           => 1,
            'data'         => $resposta,
            
        ]);
    }

    // Aquesta retorna les dades que tenim amb el codi incorrecte corresponent, en cas d'error desconegut retornar un 0
    public function incorrecte($codi_error = 0,$resposta = null)
    {
        return response()->json([
            'rc'           => $codi_error,
            'data'         => $resposta,
            
        ]);
    }
    // Convierte un request con los datos en data en un tradicional request con los datos de 'data'
    public function data_to_request(Request $request)
    {
        $request = $request->input('data');
        return new Request(json_decode($request, true));
    }
}
