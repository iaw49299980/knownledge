<?php
namespace App\Http\Controllers;
use App\User;
use App\Token_Abalit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Config; 
//para acceder a los errores, ejemplo:
//Config::get('errors_abalit.666');

/*
|--------------------------------------------------------------------------
| AuthController hereda funcions de AbalitController!!!! 
|--------------------------------------------------------------------------
|
*/


class AuthController extends AbalitController
{
    public function signup(Request $request)
    {
        $request = $this->data_to_request($request);

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {

            return $this->incorrecte(0,$validator->errors());
        }

        $user = new User();
        $user->name             =   $request->input('name');
        $user->email            =   $request->input('email');
        $user->password         =   Hash::make($request->input('password'));
        $user->save();

        return $this->correcte();
    }
    public function login(Request $request)
    {
        // los datos del login se encuentran en el campo data de la request
        // así que los recogemos del valor dara
        $request = $this->data_to_request($request);
        if (isset($request->google_id) && !empty($request->google_id)) {
            $credentials = [
                'email'     =>  $request->email,
                'password'  =>  $request->google_id
            ];
            $exists = User::where([
                'email' => $request->email,
                'google_id' => $request->google_id
            ])->get();
    
            if(empty($exists) || is_null($exists) || count($exists) == 0) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required|string',
                    'email' => 'required|string|email|unique:users',
                    'google_id' => 'required',
                ]);
        
                if ($validator->fails()) {
        
                    return $this->incorrecte(0,$validator->errors());
                }
                
                // Creamos usuario

                $user = new User();
                $user->name             =   $request->name;
                $user->email            =   $request->email;
                $user->google_id        =   $request->google_id;
                $user->password         =   Hash::make($request->google_id);
                $user->save();
            } 

        } else {
            $credentials = [
                'email'     =>  $request->email,
                'password'  =>  $request->password
            ];
        }

        // hacemos check a la bd
        if (!Auth::attempt($credentials))
        {
            return $this->incorrecte(990);

        }
        // ahora que tenemos el usuario logeado como estamos en el login de API
        // vamos a asigarle un token
        $user = auth()->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        $token->save();

        $respuesta = [
            'user'         => $user,
            'access_token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => Carbon::parse(
                $tokenResult->token->expires_at)
                    ->toDateTimeString()
        ];


        return $this->correcte($respuesta);
    }



    public function logout(Request $request)
    {
        // obtenemos el usuario
        $user = auth()->user();
        //buscamos su token
        $tokens = Token_Abalit::where('user_id', $user->id)
                ->where('revoked',false)
                ->first();
        // desactivamos el token
        $tokens->revoked = true;
        $tokens->save();
        // cerramos sesión
        Auth::logout();
        $resposta =  ['message' =>
            'Successfully logged out'];

        return $this->correcte($resposta);
    }

    // función para obtener datos del usuario con id (para pruebas)
    public function usuario(Request $request)
    {
        return response()->json(auth()->user());
    }
}
