<?php

namespace App\Http\Middleware;

use Hash;
use Auth;
use DateTime;
//use App\User;
//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Passport\TokenRepository;
use League\OAuth2\Server\ResourceServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Closure;
use App\User;

/*
|--------------------------------------------------------------------------
| CheckApiAbalit
|--------------------------------------------------------------------------
|
| Este middleware pretende ser una plantilla para Abalit dónde se
| verifica el acces_token si un usuario esta logeado.
| Esto se apoya en el uso de 'passport' de Laravel
| Este por tanto solo tendrá finalidad para la api.
|
*/

class CheckApiAbalit
{
    private $myNumber;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

     protected $server;
     protected $tokens;




    public function __construct(ResourceServer $server, TokenRepository $tokens) {
        $this->server   = $server;
        $this->tokens   = $tokens;
        // ariable global
        $this->myNumber = 0;

        //$this->user = null;

    }


    /*
    |--------------------------------------------------------------------------
    | Handle
    |--------------------------------------------------------------------------
    |
    */

    public function handle($request, Closure $next)
    {

        // validamos el token
        if($this->validateToken($request))
        {
          // Si estamos bien validados refrescamos el usuario al request
          $request = $this->addUserToRequest($request);
          // continuamos y salimos del filtro
          return $next($request);
        } else {
          //devolvemos mensaje error
          return $this->incorrecte(666);
        }
        

    }



    //Aquesta funció retorna les dades que tenim amb codi correcte 200
    public function correcte($resposta = null)
    {
        return response()->json([
            'rc'           => 1,
            'data'         => $resposta,

        ]);
    }

    //Aquesta retorna les dades que tenim amb el codi incorrecte corresponent, en cas d'error desconegut retornar un 0
    public function incorrecte($codi_error = 0,$resposta = null)
    {
        return response()->json([
            'rc'           => $codi_error,
            'data'         => $resposta,

        ]);
    }
    // Convierte un request con los datos en data en un tradicional request con los datos de 'data'
    public function data_to_request(Request $request)
    {
        $request = $request->input('data');
        return new Request(json_decode($request, true));
    }

    // validamos el token dado
    public function validateToken(Request $request) {

        $psr = (new DiactorosFactory)->createRequest($request);

        try {
            $psr = $this->server->validateAuthenticatedRequest($psr);


            $token = $this->tokens->find(
                $psr->getAttribute('oauth_access_token_id')
            );
            //asiganmos a la variable global el id del usuario
            $this->myNumber = $token->user_id;
            $currentDate = Carbon::now();
            $tokenExpireDate = new DateTime($token->expires_at);
            //si el token está caducado revolvemos error
            if ($currentDate > $tokenExpireDate)
            {
              return false;
            }
            else {
                // actualizamos la caducidad del token si en menos de 1 dia va a caducar
                if ($currentDate->addDays(1)>$token->expires_at)
                {
                  $token->expires_at = $token->expires_at->addDays(1);
                  $token->updated_at = Carbon::now();
                  $token->save();
                }

                return true;
            }
        } catch (OAuthServerException $e) {
            return false;
        }
    }

    // el usuario que nos ha desvelado el token deve ser guardado en el request y además en el usuario auth
    private function addUserToRequest(Request $request)
    {
        //obtenemos el usuario
        $user = User::findOrFail($this->myNumber);
        $request->merge(['user' => $user]);
        $request->setUserResolver(function () use ($user) {
            return $user;
        });
        // asiganmos el usuario auth
        Auth::setUser($user);
        // devolvemos nuestro request pero ademas con el usuario autenticado
        return $request;
    }

    // public function login(Request $request){
    //     $rules = array(
    //             'username'=>'required|email',
    //             'password'=>'required|alpha_dash',
    //     );
    //
    //     $this->validate($request, $rules);
    //
    //     if(Auth::attempt(array('email' => $request->username, 'password' => $request->password), true)){
    //         $access_token = Auth::user()->createToken('Access Token')->accessToken;
    //
    //         return json_encode(array('access_token' => $access_token));
    //     }
    //     else{
    //         return json_encode(array('login_error' => 'Deze combinatie van gebruikersnaam en wachtwoord bestaat niet.'));
    //     }
    // }

}
