# Laravel

## Instalación

1. Instalamos composer.
2. composer create-project laravel/laravel aprendiendo-laravel "5.8.\*" --prefer-dist
3. Nos descargamos las extensiones de php que se requiera. Son .dll y van en la carpeta ext de php.
4. En el php.ini activamos dichas extensiones añadiendo una linea en el apartado extensiones parecida a:

   ```
   extension=php_ctype
   ```

5. Activamos la opcion rewrite_module del apache.
6. Reiniciamos el xampp/wampp.
7. Para comprovar que funciona abrimos en el navegador su carpeta public.
8. Creamos un host virtual (tutorial en carpeta otros)

## Rutas

1. En la carpeta routes.

```
Route::get('/mostrar-fecha/{fecha?}', function($fecha = date('d-m-Y')) {
    $titulo = "Estoy mostrando la fecha";
    return view('mostrar-fecha', array(
        'titulo' => $titulo
    ))->where(array(
        'fecha' => '[a-zA-Z]+'
    ));
});
```

2. Parametros entre {}. Para que sea opcional poner un ? y poner un valor por defecto al parametro que recibe la funcion anonima.
3. Si el archivo de la vista esta dentro de una carpeta se pone el nombre de la carpeta primero punto nombre archivo. carpeta.archivo
4. Para poner condiciones a los parametros se añade el where a la funcion view.
5. Rutas a un controlador

   ```
    Route::get('peliculas', 'PeliculaController@index');
   ```

6. Metodos http:
   1. GET: Conseguir datos
   2. POST: Guardar datos.
   3. PUT: Actualizar recursos.
   4. DELETE: Eliminar recursos.
7. Para linkear entre rutas
   ```
   <a href="{{  route('carpeta.archivo', ['parametro' => 1]) }}"></a>
   ```
8. Crear un grupo de rutas. Para cuando en una carpeta hay varias vistas.
   ```
    Route::group(['prefix']=>'carpeta'], function () {
        Route::get('nombreRuta', 'NombreControlador@Accion');
        Route::get('nombreRuta', 'NombreControlador@Accion');
    })
   ```

## Consola Artisan

Se encuentra en la raiz del proyecto.
Desde una terminal.
Comandos interesantes:

1. php artisan route:list
2. php artisan make:controller nuevoControlador
3. php artisan make:migration nombreMigracion --table=nombreTabla
4. php artisan migrate
5. php artisan migrate:rollback
6. php artisan migrate:refresh
7. php artisan make:seed nombre_seed
8. php artisan db:seed --class=nombre_seed

## Vistas

1. Están en la carpeta /resources/views
2. Son extension .blade.php

## Blade

1. Se puede usar la sintaxis php
2. Para mostrar una variable
   ```
   {{ $variable }}
   ```
3. Comentario
   ```
   {{-- ESTO ES UN Comentario}}
   ```
4. Mostrar cuando existe
   ```
   {{ $variable or 'No existe la variable' }}
   ```
5. Estructura de control
   ```
   @if ($variable)
       El titulo existe y es este: {{ $variable }}
   @elseif(count($listado) >= 5)
       Grande
   @else
       El titulo no existe
   @endif
   ```
6. Iteraciones.

   ```
    @for ($i = 0; $i<20; $i++)
        El numero es {{ $i }}
    @endfor

    @while($variable)
        Hola que tal
        <?php $contador++ ?>
    @endwhile

    @foreach($variables as $variable)
        {{$variable}}
    @endforeach
   ```

7. Se puede hacer includes en las vistas
   ```
    @include('carpeta.archivo')
   ```

## Controladores

1. php artisan make:controller nuevoControlador
2. Controlador --resource, tiene las rutas predefinidas

   ```
    php artisan make:controller UsuarioController --resource

    Route::resource('usuario', 'UsuarioController');
   ```

3. Linkear entre controladores
   ```
   <a href="{{ action(UsuarioControlador@Login) }}"></a>
   ```
4. Redirigir a controlador
   ```
   return redirect()->action(UsuarioControlador@Login);
   ```
5. Redirigir a ruta
   ```
   return redirect('ruta');
   ```

## Formularios

1. Usaremos la clase Request por lo tanto la cargamos en el controlador donde vayamos a recibir los datos.
   ```
   use Illuminate\Http\Request;
   ```
2. La funcion que vaya a recibir los datos tendrá como parametro:
   ```
   Request $request
   ```
3. Para sacar los valores que te pasa el request;
   ```
   $nombre = $request->input('nombreDelInput');
   ```
4. En la action del formulario ponemos:
   ```
   {{ action('NombreController@funcionDelControlador) }}
   ```
5. Hay que añadirle proteccion contra ataques csrf, por lo tanto justo debajo de la apertura del form, debemos poner la siguiente funcion
   ```
   {{ csrf_field() }}
   ```

## Conexion BBDD

1. En el archivo .env
   ```
   DB_DATABASE=nombre_base_datos
   DB_USERNAME=nombre_usuario
   DB_PASSWORD=null
   ```

## Consultas BBDD

1. Cargamos el objeto DB
   ```
    use Iluminate\Supoort\Facades\DB;
   ```
2. Select

   ```
    $datos = DB::table('nombre_tabla')->get(); -- Select All
    $dato = DB::table('nombre_tabla')->where('campo', 'operador', 'condicion')->first(); -- Select where
   ```
