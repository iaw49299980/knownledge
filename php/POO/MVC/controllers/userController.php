<?php
/**
 *
 */
require_once 'models/User.php';
class userController
{
  function index() 
  {
    require_once 'views/layout/login.php';
  }
  function signUp() 
  {
    $save = false;
    if (isset($_POST['name']) && isset($_POST['password']))
    {
      $nickName = $_POST['name'];
      $password = $_POST['password'];
      $user = new User();
      $user->setUsuario($nickName);
      $user->setPassword($password);
      $save = $user->save();
    }
    if ($save)
    {
      $_SESSION['signUp'] = true;
    }
    else
    {
      $_SESSION['signUp'] = false;
    }
    header('Location:'. base_url);
  }

  function login() 
  {
    $login = false;
    if (isset($_POST['name']) && isset($_POST['password'])) {
      $nickName = $_POST['name'];
      $password = $_POST['password'];
      $user = new usuario();
      $user->setUsuario($nickName);
      $user->setPassword($password);
      $login = $user->login();
    }
    if (is_object($login))
    {
      $_SESSION['user'] = $login;
      header('Location:'.base_url.'startMenu/menu');
    }
    else
    {
      $_SESSION['usuario'] = false;
      header('Location:'.base_url);
    }
  }
}
