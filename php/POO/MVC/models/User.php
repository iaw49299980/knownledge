<?php
/**
 *
 */
class User
{
  private $id;
  private $nickName;
  private $password;
  // Conexion base de datos
  private $db;

  function __construct()
  {
    $this->db = Database::connect();
  }

  public function getId()
  {
      return $this->id;
  }
  public function setId($id)
  {
      $this->id = $this->db->real_escape_string($id);
  }
  public function getNickName()
  {
      return $this->nickName;
  }
  public function setNickName($nickName)
  {
      $this->nickName = $this->db->real_escape_string($nickName);
  }
  public function getPassword()
  {
      $passwordHashed = password_hash($this->password, PASSWORD_BCRYPT, ['cost' => 4]);
      return $passwordHashed;
  }
  public function setPassword($password)
  {

      $this->password = $password;
  }

  public function save()
  {
      $sql = "INSERT INTO user VALUES (NULL,
                                      '{$this->getNickName()}',
                                      '{$this->getPassword()}')";
      $save = $this->db->query($sql);
      return $result = $save ? true : false;
  }

  public function login()
  {
    $result = false;
    // Comprovar si existe el Usuario
    $sql = "SELECT * FROM user WHERE name = '$this->nickName'";
    $login = $this->db->query($sql);


    if ($login && $login->num_rows == 1)
    {
      $user = $login->fetch_object();

      // Verificar contraseña
      $verify = password_verify($this->password, $user->password);
      $verify ? $result = $user : $result = false;
    }
    return $result;
  }

}
