<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;

class PassportController extends Controller
{
    /**
     * Handles Registration Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        /*
        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);*/
        $json = $request->get('json', null);
        $params = json_decode($json);
        $user = User::create([
            'name' => $params->name,
            'email' => $params->email,
            'password' => bcrypt($params->password)
        ]);

        $token = $user->createToken($request->email)->accessToken;
 
        return response()->json([
            'rc' => '',
            'user' => $user,
            'token' => $token
        ], 200);
    }
 
    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $json = $request->get('json', null);
        $params = json_decode($json);
        if (isset($params->google_id) && !empty($params->google_id)) {
            $existsUser = User::where('google_id', $params->google_id)->get();
            if ($existsUser->isEmpty()) {
                $existsEmail = User::where('email', $params->email)->get();
                if ($existsEmail->isEmpty()) {
                    DB::table('users')->insert(
                        [
                            'google_id' => $params->google_id,
                            'email' => $params->email, 
                            'name' => $params->name,
                            'password' => bcrypt($params->google_id)
                        ]
                    );
                }
            } 
            $credentials = [
                'email' => $params->email,
                'google_id' => $params->google_id
            ];
        } else {
            $credentials = [
                'email' => $params->email,
                'password' => $params->password
            ];
        }
 
        if (auth()->attempt($credentials)) {
            $token = auth()->user()->createToken($params->email)->accessToken;
            return response()->json([
                'rc' => '',
                'user' => auth()->user(),
                'token' => $token
            ], 200);
        } else {
            return response()->json(['error' => 'UnAuthorised'], 401);
        }
    }
 
    /**
     * Returns Authenticated User Details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function details()
    {
        return response()->json(['user' => auth()->user()], 200);
    }
}
