## Mongo DB

* [Comparaciones SQL vs MongoDB](https://docs.mongodb.com/manual/reference/sql-comparison/)


|PostgreSQL|MongoDB|
|--|--|
|\l|show databases|
|\d|show collections|
|\c|use database|
|INSERT|db.client.insert([{nom:'Carlos'},{nom:'Maria'}])|
|SELECT|db.client.find()|
|Fila|Document|
|\x|db.client.find().pretty()|
|`psql -f 'arxiu.sql' database`| `mongoimport --db database --collection coleccion --jsonArray students.json` / --file script.js |



## INSERT
```
db.client.insert(
{
	dni:'47999217B',
	nom:'Carlos Esteban',
	adreça:
		{
			carrer:'Albert LLanas',
			pis:'36',
			porta:'1º 3ª'
		},
			aficions:
				[
					'Formula 1',
					'mountainbike',
					'futbol'
				]
}
)
```
Insertar varios documentos:
```
db.emp.insert(
	[{
		empno:'123',
		ename:'Carlos',
		sal:6000,
		comm:300,
		deptno:'10'
	},
	{
		empno:'321',
		ename:'Roger',
		sal:5000,
		comm:500,
		deptno:'10'
	}]
)
```

## SELECT / FIND
```
db.client.find(
	{}, //query WHERE
	{nom:1,adreça:1} //projection SELECT
)
```

## DELETE / REMOVE

|Tipo|Crear|Borrar| Borrar contenido|
|----------|--------------------------|------------------|---|
|col·lecció|db.createCollection('...')|db.coleccion.drop()|db.collection.remove({})|
|BD|use + insert coleccion|db.dropDatabase()  |    x     |

```
db.client.remove({ciutat:/barcelona/i})

```
## UPDATE
* [Documentación UPDATE MongoDB](https://docs.mongodb.com/manual/reference/method/db.collection.update/)
* [Operaciones de actualización de datos](https://charlascylon.com/2013-07-18-tutorial-mongodb-operaciones-de-actualizaci%C3%B3n-de)

Descpripción:
```
db.collection.update(
   <query>,
   <update>,
   {
     upsert: <boolean>,
     multi: <boolean>,
     writeConcern: <document>,
     collation: <document>,
     arrayFilters: [ <filterdocument1>, ... ]
   }
)
```
Ejemplo:
```
//empleado tenga un salario de 3000€
db.emp.update(
    {ename:/carlos/i},
    {ename:'Jordi', sal:3000}
) //Se carga todo lo que había en el documento y añade estas claves

db.emp.update(
    {ename:/carlos/i},
    {$set: {sal:"3000"}}
) //Cambia los valores de salario unicamente sin tocar las otras claves

//Afegir un camp telèfons i posar un fixe i un mòbil
db.emp.update(
    {ename:/carlos/i},
    {$set: {telefons:[932843835,611467514]}}
) //Añade claves al documento (DDL)
```

#### UPSERT

Si hay un documento que coincíde hace un update, si no existe hace un insert.
```
db.emp.update(
		{ename: /Aritz/i},
		{
			empno:'456',
			ename:'Aritz',
			sal:5000,
			comm:100,
			deptno:'10'
		},
		{upsert: true}
)
```
#### MULTI

Añade los cambios a todos los documentos que coincidan con la condición.

```
db.emp.update(
	{},
	{$unset:{subordinats:1}},
	{multi:true}
)
```


#### Operadores UPDATE

* `$set`

	Ejemplo:
	Posar un nou camp que es digui subordinats on posarem varios empleats que siguin subordinats
	```
	db.emp.insert(
		[{
			empno:'123',
			ename:'Carlos',
			sal:6000,
			comm:300,
			deptno:'10',
			subordinats: ['Pep','Maria']
		},
		{
			empno:'321',
			ename:'Roger',
			sal:5000,
			comm:500,
			deptno:'10',
			subordinats: ['Pep','Maria']
		}]
	)
	```
	```
	db.emp.update(
		{},
		{$set:{"subordinats.1":'Joan'}}
	)
	```
	Añadir subordinados el primer trabajador:
	```
	db.emp.update(
		{ename:/carlos/i},
		{$set:{subordinats: []}}
	)
	```
* `$unset`

	Ejemplo:
	Reduccio de plantilla, no hi ha subordinats
	```
	db.emp.update(
		{},
		{$unset:{subordinats:1}},
		{multi:true}
	)
	```
* `$inc` Incrementa:

	Ejemplo: Aumenta 500€ el salario y resta 200€ de comisiones
	```
	db.emp.update(
	    {ename:/carlos/i},
	    {$inc: {sal:500,comm: -200}}
	)
	```
* `$mul` Multiplica:

	Aumenta a todos un 20% el sueldo:
	```
	db.emp.update(
			{},
			{$mul: {sal:1.20}},
			{multi:true}
	)
	```

#### Operadores UPDATE Arrays

* `$push`

	Añade un valor o varios a un array.

	Ejemplo:
	Añadir un subordinado:

	```
	db.emp.update(
		{ename:/carlos/i},
		{$push:{subordinats: 'Alex'}}
	)
	```
	Añadir varios subordinados: `$each`
	```
	db.emp.update(
		{ename:/carlos/i},
		{$push:{subordinats: {$each:['Alejandro','Maria','Erik']}}}
	)
	```
* `$pull`, `$pullAll`

	Elimina un valor o varios a un array.

	Ejemplo:
	Eliminar un subordinado:

	```
	db.emp.update(
		{ename:/carlos/i},
		{$pull:{subordinats: 'Alex'}}
	)
	```
	Eliminar varios subordinados: `$in`

	```
	db.emp.update(
		{ename:/carlos/i},
		{$pull:{subordinats: {$in:['Alejandro','Maria','Erik']}}}
	)
	```
	o
	```
	db.emp.update(
		{ename:/carlos/i},
		{$pullAll:{subordinats: ['Alejandro','Maria','Erik']}}
	)
	```
* `$pop`
	Elimina el primer elemento (**-1**) o el último(**1**) de un array

	Ejemplo:

	```
	db.emp.update(
		{ename:/carlos/i},
		{$pop:{subordinats: 1}}
	)
	```
* `$addToSet`

	Es igual que un `$push` pero controla que no haya repetidos.

	Ejemplo:
	```
	db.emp.update(
		{ename:/carlos/i},
		{$addToSet:{subordinats: 'Joan'}}
	)
	```

#### Operadores FIND Arrays

* `$in`

	Selecciona el documento donde el valor de una fila es igual a cualquier valor de un array

	Ejemplo:

	Mostrar el nombre, el salario y el departamento de los empleados que trabajan en el departamento 10 o 30

	```
	db.emp.find(
		{deptno: {$in: [10,30]}},
		{ename:1, sal:1,deptno:1, _id:0}
		)
	```
* `$nin`

	Selecciona el documento donde el valor de una fila es igual a cualquier valor de un array

	Ejemplo:

	Mostrar el nombre, el salario y el departamento de los empleados que trabajan en el departamento 10 o 30

	```
	db.emp.find(
		{deptno: {$nin: [10,30]}},
		{ename:1, sal:1,deptno:1, _id:0}
		).pretty()
	```
* `$exists`

	Mostrar trabajadores con subordinados

	```
	db.emp.find(
		{subordinats: {$exists: 1}}
		).pretty()
	```
* `db.collection.aggregate()`
	Campos calculados

	Ejemplo:
	Mostrad el nombre del empleado, salario, comisión y la `suma` de los dos

	```
	db.emp.aggregate(
		[
			{$project: {_id:0, ename:1, sal:1, comm:1,
				total: {$add: ["$sal", "$comm"]}}
			}
		]
	)
	```
	Mostrad el nombre del empleado, salario, comisión y la `resta` de los dos
	```
	db.emp.aggregate(
		[
			{$project: {_id:0, ename:1, sal:1, comm:1,
				total: {$subtract: ["$sal", "$comm"]}}
			}
		]
	)
	```
	Mostrad el nombre del empleado, salario, comisión y la `multiplicacion` de los dos
	```
	db.emp.aggregate(
		[
			{$project: {_id:0, ename:1, sal:1, comm:1,
				total: {$multiply: ["$sal", "$comm"]}}
			}
		]
	)
	```
	Mostrad el nombre del empleado, salario, comisión y la `división` de los dos
	```
	db.emp.aggregate(
		[
			{$project: {_id:0, ename:1, sal:1, comm:1,
				total: {$divide: ["$sal", "$comm"]}}
			}
		]
	)
	```
	Group by
	```
	db.orders.aggregate([
                     { $match: { status: "A" } },
                     { $group: { _id: "$cust_id", total: { $sum: "$amount" } } },
                     { $sort: { total: -1 } }
                   ])
	```

## METODOS
```
.pretty()
.count()
.limit(n)
.sort({key:value, key:value...}) //Value 1 ASC Value -1 DESC
DBQuery.shellBatchSize=300 // mostrar cantidad de objetos
```

#### Ejercicio
Mostrar les pelis que començen per "L":

```
db.videoclub.find({Titol:/^l/i})
```


## OPERADORES LÓGICOS

[Query Selectors Mongo](https://docs.mongodb.com/manual/reference/operator/query/)

#### AND

Se usan la comas `,` como AND o `$AND`
```
db.airports.find({country:'Spain',state:'Balearic Islands'}).pretty()
```
Mostrar aeropuertos de españa y de las islas baleares:
```
db.airports.find(
	{
		$and:[{country:/spain/i},{state:/balearic islands/i}]
	}
)

```

#### OR

```
db.coleccio.find(
	{
		$or:[{c1},{c2}...]
	}
)
```
Numero d'aeroports que hi ha a França i a Espanya:

```
db.airports.find(
	{
		$or:[{country:/spain/i},{country:/france/i}]
	}
).count()
```

#### NOT

```
```

#### NOT IN

```
```

#### IN

```
```
#### IGUAL

####
