use students

// Buscar los estudiantes de género masculino (2895)

db.students.find({gender: 'H'}).count()

// Buscar los estudiantes de género femenino (348)

db.students.find({gender: 'M'}).count()

// Buscar los estudiantes nacidos en el año 1993 (97)

db.students.find({birth_year: 1993}).count()

// Buscar los estudiantes de género masculino y nacidos en el año 1993 (81)

db.students.find({$and: [{gender: 'M'}, {birth_year: 1993}]}).count()

// Buscar los estudiantes nacidos después del año 1990 (289)

db.students.find({birth_year: {$gt: 1990}}).count()

// Buscar los estudiantes nacidos antes o en el año 1990 (2954)

db.students.find({birth_year: {$lte: 1990}}).count()

// Buscar los estudiantes nacidos en la década de los 90 (387)

db.students.find({
	$and: [
		{birth_year: {$gte: 1990}},
		{birth_year: {$lt: 2000}}
	]
}).count()


// Buscar los estudiantes nacidos en la década de los 80 (936)

db.students.find({
    $and: [
        {birth_year: {$gte: 1980}},
        {birth_year: {$lt: 1990}}
    ]
}).count()


// Buscar los estudiantes de género femenino nacidos en la década de los 90 (48)

db.students.find({
    $and: [
		{gender: 'M'},
        {birth_year: {$gte: 1990}},
        {birth_year: {$lt: 2000}}
    ]
}).count()


// Buscar los estudiantes de género masculino nacidos en la década de los 80 (851)

db.students.find({
    $and: [
		{gender: 'H'},
        {birth_year: {$gte: 1980}},
        {birth_year: {$lt: 1990}}
    ]
}).count()


// Buscar los estudiantes que no han nacido en el año 1985 (3147)

db.students.find({
	birth_year: {$ne: 1985}
}).count()

// Buscar aquellos estudiantes que hayan nacido en el año 1970, 1980 o 1990 (293)

db.students.find({
    $or: [
        {birth_year: 1970},
        {birth_year: 1980},
        {birth_year: 1990}
    ]
}).count()

// Buscar aquellos estudiantes que no hayan nacido en el año 1970, 1980 o 1990 (2950)

db.students.find(
     {birth_year: {$nin: [1970, 1980, 1990]}}
).count()

// Buscar los estudiantes nacidos en año par (1684)

db.students.find({
		birth_year: {$mod: [2, 0]}
}).count()

// Buscar los estudiantes nacidos en año impar (1559)

db.students.find({
		birth_year: {$mod: [2, 1]}
}).count()


// Buscar estudiantes nacidos en año par de la década de los 70 que sean hombres (403)

db.students.find({
	$and: [
		{gender: 'H'},
		{birth_year: {$gte: 1970}},
		{birth_year: {$lt: 1980}},
		{birth_year: {$mod: [2, 0]}}
	]
}).count()


// Buscar los estudiantes que tengan teléfono auxiliar (679)

db.students.find({
	phone_aux: {$exists: true}
}).count()

// Buscar los estudiantes que no tengan teléfono auxiliar (2564)

db.students.find({
    phone_aux: {$exists: false}
}).count()


// Buscar los estudiantes que no tengan segundo apellido (421)

db.students.find({
    lastname2: {$exists: false}
}).count()

// Buscar los estudiantes que tengan teléfono auxiliar y solo un apellido (71)

db.students.find({
		$and: [
				{phone_aux: {$exists: true}},
				{lastname2: {$exists: false}}
}).count()


// Crea un nuevo documento en la colección, pero en lugar de utilizar el método insert() usad el método save(). ¿Hay alguna diferencia?

db.students.save({
	name: 'Angus',
	lastname1: 'Young',
	gender: 'H',
	email: 'acdc@gmail.com',
	phone: '678.123.456',
	birth_year: 1955
})

db.students.save({
  _id: 1,
	name: 'Malcolm',
	lastname1: 'Young',
	gender: 'H',
	email: 'acdc@gmail.com',
	phone: '678.123.456',
	birth_year: 1953
})
// Y crea otro documento más, utilizando la última sentencia, cambiando algún campo (excepto el _id). ¿Qué ocurre?

db.students.find(
	{name:/malcolm/i}
).pretty()

db.students.save({
  _id: 1,
	name: 'Axl',
	lastname1: 'Rose',
	gender: 'H',
	email: 'acdc@gmail.com',
	phone: '678.123.456',
	birth_year: 1962
})

db.students.find(
	{name:/malcolm/i}
).count()

print ('\n\n¿ Malcolm ha desaparecido ??\nSiii, ha sido sustituido por Axl\n\n')

db.students.find(
	{name:/axl/i},
	{name:1, lastname1:1, _id:0}
).pretty()
// ¿Cómo puedo hacer un “like”? Tenemos dos maneras de hacerlo. Pon un ejemplo de cada  una.

	// En operaciones de búsqueda se admiten expresiones regulares con la sintaxis de expresiones regulares de javascript como valor. Se puede hacer directamente o utilizado el operador $regex, el cual nos permite utilizar patrones de expresiones regulares en lugares donde por defecto no se interpretarían, como en cláusulas $in.

db.students.find({
	name: /^d/i
}).count()


// Buscar los estudiantes cuyo email termine en .net (48)

db.students.find({email: /\.net$/i}).count()

// Buscar los estudiantes cuyo email termine en .org (16)

db.students.find({email: /\.org$/i}).count()

// Buscar los estudiantes cuyo teléfono empiece por 622 (201)

db.students.find({phone: /^622/}).count()

// 26. Buscar los estudiantes cuyo nombre empiece y termine por vocal (346)

db.students.find(
	{name: /^[aeiouàáèéìíòóùú].*[aeiouàáèéìíòóùú]$/i},
	{name: 1, _id : 0}
).pretty().count()

// 27. Muestra los estudiantes cuyo nombre ni empiece ni termine por vocal (1515)

db.students.find(
	{name: /^[^aeiouàáèéìíòóùú].*[^aeiouàáèéìíòóùú]$/i},
	{name: 1, _id : 0}
).pretty().count()


// Buscar estudiantes cuyo nombre sea compuesto (470)

db.students.find(
	{	name: /.+\s.+/}
).pretty().count()

db.students.find(
	{	name: /^.* .*/ }
).count()

db.students.find(
	{	name: /\s/ },
	{ name :1, _id : 0 }
).pretty().count()

// Buscar los estudiantes con nombre más largo de 13 caracteres (138)

db.students.find(
	{	name: /.{13}.*/}
).count()

// Buscar los estudiantes con 3 o más vocales en su nombre (705)

db.students.find(
	{	name: /(.*[aeiou]){3}.*/i}
).count()
