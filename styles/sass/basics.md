# Sass

## Instalar

- En VS code instalar *** Live Sass *** 
- Tambien se puede usar node js usando webpack.

1. Creamos una carpeta donde pondremos el style.scss y al guardar nos creará un archivo style.css.


## ¿Que podemos hacer con SASS?

- Podemos crear variables

    ```
    $primaryColour: #fff;
    ```

- Podemos niar elementos.
  ```
    header {
        display: flex;
        nav {
            ul {

            }
        }
        .logo {

        }
    }
  ```
- Para hacer el hover:
    ```
    button {
        color: red;
        &:hover {
            color: white;
        }
    }
    ```
- Podemos incluir otros archivos a nuestro scss principal.
  
    ```
    @import './header' 
    ```
- Podemos tener fragmentos de codigos que se repiten en una funcion.
  
    ```
    @mixin flexCenter($bk) {
        display: flex;
        background: $bk
    }
    header {
        @include flexCenter(black);

    }
    ```

- Podemos extender una clase a otra.
  
    ```
    .menu {
        @extends nav;
    }
    ```
- Podemos hacer calculos.

    ```
    .elementoFlex {
        width: 100%/6;
    }
    ```





## Buenas praxis

- Tener un archivo para cada seccion grande.
- Tener un archivo con las variables.
- Tener un archivo con los mixins.