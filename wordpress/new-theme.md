# Wordpress

## Instalar wordpress

1. Descargamos wordpress.
2. Le cambiamos el nombre de la carpeta por el del proyecto.
3. Lo ponemos en htdocs.
4. Creamos una bbdd.
5. Instalamos wordpress.

## Host Virtual (opcional)

1. Ver tutorial de [Host Virtual](../otros/host-virtual.md)
2. Entramos a phpmyadmin a la bbdd del proyecto.
3. Vamos a la tabla wp-options.
4. Cambiamos las filas ** siteurl ** y ** home **

## Enlaces amigables

1. Vamos al panel de control.
2. Settings.
3. Enlaces Permanentes.
4. Seleccionamos: Nombre de la entrada

## Nuestro Tema

1. En la carpeta wp-content/themes creamos una nueva carpeta con el nombre de nuestra plantilla.
2. Creamos los siguientes archivos:
   1. index.php
   2. style.css
      ```
          /*
          Theme Name: Maxims Moda
          Author: Pablo Hernando
          Author URI: https://proyectosphm.com/
          */
      ```
   3. Nuestra miniatura: screenshot.png
   4. header.php
   5. footer.php
   6. functions.php
   7. page.php
   8. single.php
   9. archive.php
   10. front-page.php
   11. 404.php
   12. app.js

## Funciones interesantes

```
wp_head();
wp_footer();
body_class();
get_header();
get_footer();
woocommerce_content();

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php the_content() ?>
            <?php endwhile; ?>
        <?php endif; ?>

```