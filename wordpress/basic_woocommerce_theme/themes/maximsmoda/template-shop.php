<?php get_header();

/*
    Template Name: Shop Page
*/ 

?>
<div class="container">
    <div class="content">
        <h1><?php the_title()?></h1>    
        <?php 
        if ( have_posts() ) :
            while ( have_posts() ) : the_post();
        ?>
            <?php the_content() ?>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>