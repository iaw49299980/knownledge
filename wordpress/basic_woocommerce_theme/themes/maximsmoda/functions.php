<?php
// Añadir custom stylesheet

function load_stylesheet() 
{

    wp_register_style('stylesheet', get_template_directory_uri() . '/style.css', '', 1, 'all');
    wp_enqueue_style('stylesheet');
    wp_register_style('custos_scss', get_template_directory_uri() . '/src/css/app.css', '', 1, 'all');
    wp_enqueue_style('custos_scss');

}
add_action('wp_enqueue_scripts', 'load_stylesheet');

// Añadir custom js

function load_javascript() 
{

    wp_register_script('custom', get_template_directory_uri() . '/src/js/app.js', 'jquery', 1, true);
    wp_enqueue_script('custom');

}

add_action('wp_enqueue_scripts', 'load_javascript');

// Add support
add_theme_support('menus');
add_theme_support('thumbnails');
// Register menus 

register_nav_menus(
    array(
        'top-meu' => __('Top Menu', 'maximsmoda')
    )
);

// Widgets


// WOocommerce
function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );